/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.wallet.crypto

import java.security.MessageDigest

enum class EllipticCurve {
    ED25519
}

enum class KeyFormat {
    BASE58
}

/**
 * Calculates the SHA-256 hash of the given bytes and hashes the resulting hash again.
 *
 * @param offset the offset within the array of the bytes to hash
 * @param length the number of bytes to hash
 * @return the double-hash (in big-endian order)
 */
fun ByteArray.sha256HashTwice(offset: Int, length: Int): ByteArray {
    val digest = MessageDigest.getInstance("SHA-256")
    digest.update(this, offset, length)
    return digest.digest(digest.digest())
}


/**
 * Calculates the SHA-256 hash of the given byte range.
 *
 * @param input the array containing the bytes to hash
 * @param offset the offset within the array of the bytes to hash
 * @param length the number of bytes to hash
 * @return the hash (in big-endian order)
 */
fun ByteArray.hash(offset: Int = 0, length: Int = this.size): ByteArray {
    val digest = MessageDigest.getInstance("SHA-256")
    digest.update(this, offset, length);
    return digest.digest()
}

/**
 * Convert the given input bytes to hex.
 */
fun ByteArray.toHexString() = joinToString("") { "%02x".format(it) }

/**
 * Convert the given hex to binary.
 */
fun String.hexStringToByteArray() =
    ByteArray(this.length / 2) { this.substring(it * 2, it * 2 + 2).toInt(16).toByte() }

/**
 * Prepare bytes for verification by applying a watermark and hashing.
 */
fun ByteArray.watermarkAndHash(): ByteArray? {
    val watermark = Prefix.operation + this
    return SodiumFacade.hash(watermark, 32)
}

/**
 * @return 32 byte of hex string
 */
internal fun ByteArray.get32Hex(): String{
    return toHexString().substring(0, 64)
}
