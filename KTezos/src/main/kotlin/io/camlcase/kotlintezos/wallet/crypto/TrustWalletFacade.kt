/**
 * # Released under MIT License
 *
 * Copyright (c) 2021 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.wallet.crypto

import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import wallet.core.jni.HDWallet

/**
 * Wrapper around TrustWallet [HDWallet] creation.
 *
 * [See C++ class](https://github.com/trustwallet/wallet-core/blob/8c5608a3d7e914658fa2c5ef8a887f90455a1d95/src/HDWallet.cpp)
 * [See Trezor-crypto BIP39 code](https://github.com/trustwallet/wallet-core/blob/05375deb1b4b45d491b59ca775aa43e9e9a39b11/trezor-crypto/src/bip39.c)
 */
class TrustWalletFacade {

    init {
        System.loadLibrary("TrustWalletCore")
    }

    /**
     * Load JNI Library to the environment and create an instance of TrustWallet's [HDWallet].
     */
    fun loadCoreWallet(mnemonic: List<String>?, passphrase: String?): HDWallet {
        return when {
            mnemonic.isNullOrEmpty() -> {
                loadCoreWallet(passphrase = passphrase)
            }
            !validate(mnemonic) -> {
                throw TezosError(
                    TezosErrorType.MNEMONIC_GENERATION,
                    exception = IllegalArgumentException("The given mnemonic is not valid.")
                )
            }
            else -> {
                HDWallet(mnemonic.joinToString(" "), passphrase ?: "")
            }
        }
    }

    /**
     * Load JNI Library to the environment and create an instance of TrustWallet's [HDWallet].
     * @param strength Level of entropy to generate the mnemonic. Must be multiple of 32 and between 128 and 256. Default is [DEFAULT_MNEMONIC_STRENGTH]
     */
    fun loadCoreWallet(
        strength: Int = DEFAULT_MNEMONIC_STRENGTH,
        passphrase: String? = null
    ): HDWallet {
        return HDWallet(strength, passphrase ?: "")
    }

    /**
     * Validate that the given list of words is a valid mnemonic.
     *
     * @return true If [mnemonic] is valid, false if it's not.
     */
    fun validate(mnemonic: List<String>): Boolean {
        val joinedMnemonic = mnemonic.joinToString(" ")
        return HDWallet.isValid(joinedMnemonic)
    }

    companion object {
        private const val DEFAULT_MNEMONIC_STRENGTH = 128
    }
}
