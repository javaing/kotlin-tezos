/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.operation

import io.camlcase.kotlintezos.NetworkClient
import io.camlcase.kotlintezos.wallet.SignatureProvider
import io.camlcase.kotlintezos.core.ext.wrap
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.blockchain.BlockchainMetadata
import io.camlcase.kotlintezos.model.operation.Operation
import io.camlcase.kotlintezos.model.operation.SimulationResponse
import io.camlcase.kotlintezos.model.operation.payload.OperationPayload
import io.camlcase.kotlintezos.operation.forge.RemoteForgingService
import io.camlcase.kotlintezos.operation.forge.RemoteForgingVerifier
import io.github.vjames19.futures.jdk8.Future
import io.github.vjames19.futures.jdk8.flatMap
import io.github.vjames19.futures.jdk8.onComplete
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService

/**
 * A service which manages forging of operations given a [ForgingPolicy]. Right now it only supports remote option.
 */
class ForgingService(
    private val remoteForgingService: RemoteForgingService,
    private val executorService: ExecutorService
) {

    /**
     * Create a [RemoteForgingService] with [verifierNodeUrl].
     *
     * @param verifierNodeUrl Endpoint of a separate node for verifying forge responses. Should be different from the one used in [networkClient]
     * @param executorService to manage network requests and calculations asynchronously.
     * @param debug Log network calls
     */
    constructor(
        networkClient: NetworkClient,
        executorService: ExecutorService,
        verifierNodeUrl: String,
        debug: Boolean = false
    ) : this(
        RemoteForgingService(networkClient, verifierNodeUrl, executorService, debug),
        executorService
    )

    /**
     * Create the service with separate [NetworkClient], one for forging and another one for parsing
     * and validating the result of the forge.
     *
     * @param networkClient To connect to default node
     * @param validatorNetworkClient To connect to parsing node
     * @param executorService To manage network requests and calculations asynchronously.
     */
    constructor(
        networkClient: NetworkClient,
        validatorNetworkClient: NetworkClient,
        executorService: ExecutorService
    ) : this(
        RemoteForgingService(
            networkClient,
            RemoteForgingVerifier(validatorNetworkClient, executorService),
            executorService
        ),
        executorService
    )

    /**
     * Forge the given operation.
     * @return Completable with a String representing the result of the forge.
     */
    fun forge(
        policy: ForgingPolicy = ForgingPolicy.REMOTE,
        address: Address,
        operation: Operation,
        metadata: BlockchainMetadata,
        signatureProvider: SignatureProvider
    ): CompletableFuture<String?> {
        return Future(executorService) {
            createPayload(operation, address, metadata, signatureProvider)
        }.flatMap { forge(policy, it, metadata) }
    }


    /**
     * Forge the given operation payload.
     * @param callback To return the [SimulationResponse] asynchronously or [TezosError]
     */
    fun forge(
        policy: ForgingPolicy = ForgingPolicy.REMOTE,
        payload: OperationPayload,
        metadata: BlockchainMetadata,
        callback: TezosCallback<String>
    ) {
        forge(policy, payload, metadata)
            .onComplete(
                onFailure = { throwable ->
                    callback.onFailure(throwable.wrap())
                },
                onSuccess = { result ->
                    callback.onSuccess(result)
                })
    }

    /**
     * Forge the given operation payload.
     *
     * @return Completable with a String representing the result of the forge.
     * @throws TezosError [TezosErrorType.FORGING_ERROR] If tampering during the forge is detected.
     */
    fun forge(
        policy: ForgingPolicy = ForgingPolicy.REMOTE,
        payload: OperationPayload,
        metadata: BlockchainMetadata
    ): CompletableFuture<String?> {
        return remoteForgingService.remoteForge(payload, metadata)
    }

    private fun createPayload(
        operation: Operation,
        from: Address,
        metadata: BlockchainMetadata,
        signatureProvider: SignatureProvider
    ): OperationPayload {
        return OperationPayload(listOf(operation).asIterable(), from, metadata, signatureProvider)
    }
}

/**
 * An enum defining policies used to forge operations.
 */
enum class ForgingPolicy {
    /**
     * Always forge operations remotely on the node.
     */
    REMOTE
}
