/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.operation

import io.camlcase.kotlintezos.wallet.PublicKey
import io.camlcase.kotlintezos.wallet.SignatureProvider
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosProtocol
import io.camlcase.kotlintezos.model.blockchain.BlockchainMetadata
import io.camlcase.kotlintezos.model.operation.Operation
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.RevealOperation
import io.camlcase.kotlintezos.operation.fees.DefaultFeeEstimator
import io.camlcase.kotlintezos.operation.metadata.MetadataService
import io.github.vjames19.futures.jdk8.map
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService

/**
 * Determine if the address performing the operations has been revealed.
 */
class RevealService(
    private val metadataService: MetadataService,
    private val executorService: ExecutorService
) {

    /**
     * Determine if the address performing the operations has been revealed.
     * @param source Address which will run the [operations]
     * @return Completable with [operations] + 1 extra Reveal operation with default fees if a Reveal is needed
     */
    fun checkReveal(
        source: Address,
        operations: Iterable<Operation>,
        publicKey: PublicKey
    ): CompletableFuture<List<Operation>> {
        return metadataService.getMetadata(source)
            .map(executorService) {
                checkRevealOperation(operations, it.key, source, publicKey, it.tezosProtocol)
            }
    }

    /**
     * Determine if the address performing the operations has been revealed.
     * @param source Address which will run the [operations]
     * @return Completable with true if a Reveal operation is needed to be added to [operations] or false if it isn't.
     */
    fun needsReveal(
        source: Address,
        operations: Iterable<Operation>
    ): CompletableFuture<Boolean> {
        return metadataService.getMetadata(source)
            .map(executorService) {
                needsReveal(it.key, operations)
            }
    }

    companion object {
        fun checkRevealOperation(
            operations: Iterable<Operation>,
            source: Address,
            metadata: BlockchainMetadata,
            signatureProvider: SignatureProvider
        ): List<Operation> {
            return checkRevealOperation(
                operations,
                metadata.key,
                source,
                signatureProvider.publicKey,
                metadata.tezosProtocol
            )
        }


        /**
         * If it's not been revealed, check if any of the operations to perform requires the address to be revealed.
         * If so, prepend a reveal operation to the operations to perform.
         *
         * @param key The base58encoded public key for the address, or nil if the key is not yet revealed.
         */
        fun checkRevealOperation(
            operations: Iterable<Operation>,
            key: String?,
            source: Address,
            publicKey: PublicKey,
            protocol: TezosProtocol
        ): List<Operation> {
            val list = ArrayList<Operation>()
            if (needsReveal(key, operations)) {
                list.add(revealOperation(source, publicKey, protocol))
            }
            list.addAll(operations)
            return list
        }


        /**
         * @param key The base58encoded public key for the address, or nil if the key is not yet revealed.
         */
        private fun needsReveal(key: String?, operations: Iterable<Operation>): Boolean {
            val operationsNeedReveal = operations.firstOrNull { it.requiresReveal } != null
            val revealOperationExists = operations.firstOrNull { it.type == OperationType.REVEAL } != null
            return key.isNullOrBlank() && operationsNeedReveal && !revealOperationExists
        }

        /**
         * Create a [OperationType.REVEAL] operation
         * @param source The address to reveal.
         * @param publicKey The public key for the address being revealed.
         * @param protocol [TezosProtocol] to set default fees to the operation
         */
        private fun revealOperation(source: Address, publicKey: PublicKey, protocol: TezosProtocol): RevealOperation {
            return RevealOperation(source, publicKey, DefaultFeeEstimator.calculateFees(protocol, OperationType.REVEAL))
        }
    }

}
