/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.operation.fees

import io.camlcase.kotlintezos.core.ext.badArgumentsError
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.NanoTez
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.blockchain.BlockchainMetadata
import io.camlcase.kotlintezos.model.operation.Operation
import io.camlcase.kotlintezos.model.operation.SimulatedFees
import io.camlcase.kotlintezos.model.operation.SimulationResponse
import io.camlcase.kotlintezos.model.operation.fees.CalculatedFees
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.model.operation.payload.OperationPayload
import io.camlcase.kotlintezos.model.toTez
import io.camlcase.kotlintezos.operation.ForgingPolicy
import io.camlcase.kotlintezos.operation.ForgingService
import io.camlcase.kotlintezos.operation.SimulationService
import io.camlcase.kotlintezos.wallet.SignatureProvider
import io.camlcase.kotlintezos.wallet.SimulatedSignatureProvider
import io.camlcase.kotlintezos.wallet.crypto.hexStringToByteArray
import io.github.vjames19.futures.jdk8.map
import io.github.vjames19.futures.jdk8.zip
import java.util.concurrent.CompletableFuture
import java.util.concurrent.ExecutorService

/**
 * Provides calculated fee values for operations. Since it runs against a node it can give a better aproximate cost of
 * running a list of operations than with default values. Specially recommended for operations against smart contracts
 * that vary in cost.
 *
 * It also finds the extra network fees the manager address might need to pay (like burn fees). See [ExtraFees].
 *
 * Follows [Tezos documentation](https://tezos.gitlab.io/protocols/003_PsddFKi3.html#more-details-on-fees-and-cost-model) for calculating xtz, gas and storage costs.
 */
class FeeEstimatorService(
    private val executorService: ExecutorService,
    private val simulationService: SimulationService,
    private val forgingService: ForgingService
) {

    /**
     * @param operations List of [Operation] to estimate.
     * @param address Manager account which will pay the fees.
     * @param metadata With the latest information needed to simulate a run of the operation
     * @throws TezosError If error in simulation or forge operations
     */
    fun estimate(
        operations: List<Operation>,
        address: Address,
        metadata: BlockchainMetadata,
        signatureProvider: SignatureProvider
    ): CompletableFuture<CalculatedFees> {
        return simulateFees(operations, address, metadata, signatureProvider)
            .zip(
                getForgedOperationsSize(operations, address, metadata, signatureProvider),
                executorService
            ) { simulationResponse, operationSize ->
                calculateFees(simulationResponse, operationSize)
            }
    }

    /**
     * @param response Simulation results
     * @return CalculatedFees with a list of parsed fees for every operation and an accumulated total
     */
    private fun calculateFees(
        response: SimulationResponse,
        operationSize: Int
    ): CalculatedFees {
        val listOfFees = ArrayList<OperationFees>()
        var accumulatedFees = OperationFees.empty

        for (simulation in response.simulations) {
            val operationFees = calculateOperationFees(simulation, operationSize)
            accumulatedFees += operationFees

            listOfFees.add(operationFees)
        }
        return CalculatedFees(
            listOfFees,
            accumulatedFees,
            response.errors
        )
    }

    private fun calculateOperationFees(
        simulation: SimulatedFees,
        operationSize: Int
    ): OperationFees {
        val initialFee = calculateBakerFee(operationSize, simulation.consumedGas)

        val gasLimit = simulation.consumedGas + SafetyMargin.GAS
        val storageLimit = simulation.consumedStorage + SafetyMargin.STORAGE
        val extraFees = simulation.extraFees

        return OperationFees(initialFee, gasLimit, storageLimit, extraFees)
    }

    /**
     * @throws TezosError If Simulation service returns a null OperationFees
     */
    private fun simulateFees(
        operations: List<Operation>,
        address: Address,
        metadata: BlockchainMetadata,
        signatureProvider: SignatureProvider
    ): CompletableFuture<SimulationResponse> {
        return simulationService.simulate(operations, address, metadata, signatureProvider)
            .map(executorService) {
                // Certain errors still provide simulation information, so we can keep calculating.
                val fatalError: Boolean = !it.errors.isNullOrEmpty()
                        && (it.simulations.isEmpty() || it.simulations[0].consumedGas == 0)
                if (fatalError) {
                    throw TezosError(TezosErrorType.SIMULATION_FAILED, it.errors)
                }
                it
            }
    }

    /**
     * @throws TezosError If forge result is null
     */
    private fun getForgedOperationsSize(
        operations: List<Operation>,
        address: Address,
        metadata: BlockchainMetadata,
        signatureProvider: SignatureProvider
    ): CompletableFuture<Int> {
        fun operationSize(forgeResult: String): Int {
            // The size is the number of bytes of the complete serialized operation, i.e. including header and signature.
            // The real signature is not needed for estimation, we just use a zero byte array.
            val size = forgeResult.hexStringToByteArray() + SimulatedSignatureProvider.simulationSignature
            return size.count()
        }

        val payload = OperationPayload(operations.asIterable(), address, metadata, signatureProvider)
        return forgingService.forge(ForgingPolicy.REMOTE, payload, metadata)
            .map(executorService) {
                if (it == null) {
                    throw badArgumentsError("[FeeEstimator] Invalid parameters: { ${operations}, $address } to forge an operation")
                }
                operationSize(it)
            }
    }

    companion object {

        /**
         * Follow []Protocol 3](https://tezos.gitlab.io/protocols/003_PsddFKi3.html) formula:
         *  fees >= minimal_fees + (minimal_nanotez_per_byte * size) + (minimal_nanotez_per_gas_unit * gas)
         *
         * @param operationSize Number of bytes of the complete serialized operation, i.e. including header and signature
         * @param gas Limit set by the dry-run
         * @return Tez to pay as network fee
         */
        fun calculateBakerFee(
            operationSize: Int,
            gas: Int
        ): Tez {
            val minimalNanoTezPerByte: NanoTez = FeeConstants.FEE_PER_STORAGE_BYTE
            val minimalNanotezPerGasUnit: NanoTez = FeeConstants.FEE_PER_GAS_UNIT

            val storageFee = operationSize * minimalNanoTezPerByte
            val gasFee = gas * minimalNanotezPerGasUnit

            return FeeConstants.MINIMAL_FEE.toTez() + storageFee.toTez() + gasFee.toTez()
        }
    }
}
