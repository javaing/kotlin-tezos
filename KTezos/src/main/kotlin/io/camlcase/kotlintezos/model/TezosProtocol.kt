/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model

/**
 * Protocols supported
 */
enum class TezosProtocol(val protocolName: String) {
    BABYLON("PsBabyM1"), // Protocol 005_PsBabyM1
    CARTHAGE("PsCARTHA"), // Protocol 006_PsCARTHA
    DELPHI("PsDELPH1"), // Protocol 007_PsDELPH1
    EDO("PtEdoTez"), // Protocol 008_PtEdoTez
    FLORENCE("PsFLoren"), // Protocol 009_PsFLoren
    GRANADA("PtGRANAD"),; // Protocol 010_PtGRANAD

    companion object {
        /**
         * @param name String value returned when querying the head hash of the chain. By default, [EDO]
         */
        fun get(name: String): TezosProtocol {
            values().forEach { value ->
                if (name.startsWith(value.protocolName)) {
                    return value
                }
            }
            return GRANADA
        }
    }
}
