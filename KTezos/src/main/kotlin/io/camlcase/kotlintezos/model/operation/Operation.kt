/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.operation

import io.camlcase.kotlintezos.wallet.PublicKey
import io.camlcase.kotlintezos.data.Payload
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.SmartContractScript
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.smartcontract.michelson.MichelsonParameter

/**
 * Protocol representing all operations. Operations are first class representations of JSON object which can be forged / pre-applied / injected on the Tezos Blockchain.
 */
interface Operation : Payload {
    val type: OperationType

    /**
     * Whether the given operation requires the account to be revealed.
     */
    val requiresReveal: Boolean

    /**
     * Whether the given operation requires adding a counter value.
     */
    val requiresCounter: Boolean

    /**
     * Fees associated with the operation.
     */
    val fees: OperationFees

    /**
     * Update with new fees
     */
    fun copy(newFees: OperationFees): Operation
}

/**
 * All supported operation types.
 * Raw values of the enum represent the string the Tezos blockchain expects for the "kind" attribute when forging / pre-applying / injecting operations
 */
enum class OperationType {
    /**
     * Send an amount of [Tez] from an [Address] to another
     */
    TRANSACTION,
    DELEGATION,
    REVEAL,

    /**
     * Deploy a smart contract to the blockchain
     */
    ORIGINATION;

    companion object {
        fun get(name: String): OperationType {
            val found = values().firstOrNull { it.name.equals(name, true) }
            return found ?: TRANSACTION
        }
    }
}

/**
 * Helper class to populate an [Operation].
 */
sealed class OperationParams {
    data class Transaction(val amount: Tez, val from: Address, val to: Address) :
        OperationParams()

    data class ContractCall(
        val amount: Tez,
        val from: Address,
        val to: Address,
        val parameter: MichelsonParameter? = null,
        val entrypoint: String? = null
    ) : OperationParams()

    data class Origination(
        val source: Address,
        val script: SmartContractScript?
    ) : OperationParams()

    /**
     * @param delegate If null, it will undelegate
     */
    data class Delegation(val source: Address, val delegate: Address?) : OperationParams()
    data class Reveal(val source: Address, val publicKey: PublicKey) : OperationParams()
}
