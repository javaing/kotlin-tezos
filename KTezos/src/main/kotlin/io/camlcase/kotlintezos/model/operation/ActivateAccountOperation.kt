/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.operation

import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.operation.fees.OperationFees

/**
 * An operation which activates the given [address] with its [secret]
 */
class ActivateAccountOperation(
    private val address: Address,
    private val secret: String,
    // Won't be used
    override val type: OperationType = OperationType.TRANSACTION,
    override val requiresReveal: Boolean = false,
    override val requiresCounter: Boolean = false,
    // Won't be used
    override val fees: OperationFees = OperationFees.empty,
) : Operation {
    /**
     *  { /* Activate_account */
     * "kind": "activate_account",
     * "pkh": $Ed25519.Public_key_hash,
     * "secret": /^[a-zA-Z0-9]+$/ }
     */
    override val payload: MutableMap<String, Any>
        get() {
            val payload = HashMap<String, Any>()
            payload[BaseOperation.PAYLOAD_ARG_KIND] = PAYLOAD_ARG_KIND_ACTIVATE
            payload[PAYLOAD_ARG_PKH] = address
            payload[PAYLOAD_ARG_SECRET] = secret
            return payload
        }

    override fun copy(newFees: OperationFees): Operation {
        return ActivateAccountOperation(address, secret, fees = newFees)
    }

    companion object {
        private const val PAYLOAD_ARG_PKH = "pkh"
        private const val PAYLOAD_ARG_SECRET = "secret"
        private const val PAYLOAD_ARG_KIND_ACTIVATE = "activate_account"
    }
}
