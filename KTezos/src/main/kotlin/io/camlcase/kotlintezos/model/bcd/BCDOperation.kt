/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.bcd

import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.model.bcd.dto.BCDError
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.fees.OperationFees

/**
 * Representation of a blockchain operation based on the Better Call Dev api. Parameters are bundled for readability.
 */
interface BCDOperation {
    val kind: OperationType
    val id: String
    val hash: String
    val protocol: String
    val network: TezosNetwork
    val timestamp: String
    val status: OperationResultStatus

    /**
     * A bundle of these api fields:
     * - fee: Fee to the baker, produced block, in which the operation was included
     * - gas_limit: A cap on the amount of gas a given operation can consume
     * - storage_limit: A cap on the amount of storage a given operation can consume
     * - paid_storage_size_diff: The amount of funds burned from the sender account for used the blockchain storage (micro tez)
     */
    val fees: OperationFees?

    /**
     * List of possible errors when running the operation
     */
    val errors: List<BCDError>?
}
