/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.operation

import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.Tez
import java.math.BigInteger
import java.util.*


enum class TokenOperationType {
    /**
     * Send an amount of Token from one account to another
     */
    TRANSFER,

    /**
     * Send an amount of Tez to be exchanged for FA1.2 token
     */
    TOKEN_TO_TEZ,

    /**
     * Send an amount of FA1.2 to be exchanged for Tez
     */
    TEZ_TO_TOKEN,
    ADD_LIQUIDITY
}

/**
 * Helper class to populate an [SmartContractCallOperation].
 */
sealed class TokenOperationParams {

    data class Transfer(
        val amount: BigInteger,
        val tokenAddress: Address,
        val to: Address,
        val from: Address
    ) : TokenOperationParams()

    /**
     * @param destination Address which will receive the swapped tokens
     */
    data class TezToToken(
        val dexterAddress: Address,
        val source: Address,
        val destination: Address,
        val amount: Tez,
        val tokenAmount: BigInteger,
        val deadline: Date
    ) :
        TokenOperationParams()

    /**
     * TOKEN -> XTZ
     *
     *
     * @param dexterAllowance How many allowances for this FA1.2 token Dexter has
     * @param approves
     * @param needsApproval Add an [SmartContractCallOperation.SMART_CONTRACT_ENTRYPOINT_APPROVE] operation to the
     * list so Dexter can trade FA1.2 for the given [from] Address. Default is true.
     * You can set it to false if you need to add a custom approval operation.
     */
    data class TokenToTez(
        val dexterAddress: Address,
        val tokenAddress: Address,
        val from: Address,
        val owner: Address = from,
        val destination: Address,
        val amount: Tez,
        val tokenAmount: BigInteger,
        val deadline: Date,
        val dexterAllowance: BigInteger,
        val needsApproval: Boolean = true
    ) : TokenOperationParams()

    data class AddLiquidity(
        val dexterAddress: Address,
        val tokenAddress: Address,
        val from: Address,
        val amount: Tez,
        val minLiquidity: BigInteger,
        val maxToken: BigInteger,
        val dexterAllowance: BigInteger,
        val deadline: Date
    ) : TokenOperationParams()
}
