/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.operation.fees

import io.camlcase.kotlintezos.model.Tez
import java.util.*

/**
 * Additional fees the account will have to pay in order to run an operation.
 *
 * Wrapper for [ExtraFee] implementations so it can be operated with.
 */
data class ExtraFees(
    private val map: EnumMap<ExtraFeeType, Tez> = EnumMap(ExtraFeeType::class.java)
) {

    val asList: List<ExtraFee>
        get() {
            return map.map {
                when (it.key) {
                    ExtraFeeType.ALLOCATION_FEE -> AllocationFee(it.value)
                    ExtraFeeType.ORIGINATION_FEE -> OriginationFee(it.value)
                    else -> BurnFee(it.value)
                }
            }
        }


    fun add(extraFee: ExtraFee) {
        when (extraFee) {
            is AllocationFee -> map[ExtraFeeType.ALLOCATION_FEE] = extraFee.fee
            is OriginationFee -> map[ExtraFeeType.ORIGINATION_FEE] = extraFee.fee
            else -> map[ExtraFeeType.BURN_FEE] = extraFee.fee
        }
    }

    fun getFee(type: ExtraFeeType): Tez? {
        return map[type]
    }

    operator fun plus(other: ExtraFees): ExtraFees {
        val loopMap: EnumMap<ExtraFeeType, Tez>
        val compareMap: EnumMap<ExtraFeeType, Tez>
        if (this.map.size > other.map.size) {
            loopMap = this.map
            compareMap = other.map.clone()
        } else {
            loopMap = other.map
            compareMap = this.map.clone()
        }

        loopMap.forEach {
            val fee = compareMap[it.key] ?: Tez.zero
            compareMap[it.key] = fee + it.value
        }
        return ExtraFees(compareMap)
    }

    fun isEmpty(): Boolean {
        return map.isEmpty()
    }
}

enum class ExtraFeeType {
    BURN_FEE,
    ALLOCATION_FEE,
    ORIGINATION_FEE
}

/**
 * Additional fees removed from the account of the sender but not sent to anyone. They will be burnt on the network.
 */
interface ExtraFee {
    val fee: Tez
}

/**
 * Certain operations such as allocating space for a new wallet, incur a burn fee to prevent malicious users from spamming the network to create addresses.
 */
data class BurnFee(override val fee: Tez) : ExtraFee

/**
 * Similar to the burn fee, but charged to the sender of an operation who may not own the new address.
 */
data class AllocationFee(override val fee: Tez) : ExtraFee

/**
 * Fee for creating contracts in the blockhain
 */
data class OriginationFee(override val fee: Tez) : ExtraFee
