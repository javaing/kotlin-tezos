/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model

import io.camlcase.kotlintezos.data.Payload

/**
 * @see { https://tezos.gitlab.io/api/rpc.html#post-block-id-helpers-scripts-run-operation }
 *
 * @param code Micheline object containing the complied Michelson smart contract code as a list of maps (JSONArray)
 * @param init Micheline map of values (JSONObject) used to initialize storage
 */
data class SmartContractScript(
    val code: List<Map<String, Any?>>,
    val init: Map<String, Any?>
) : Payload {

    /**
     *  $scripted.contracts:
     *  { "code": $micheline.michelson_v1.expression,
     *  "storage": $micheline.michelson_v1.expression }
     */
    override val payload: MutableMap<String, Any>
        get() {
            val payload = HashMap<String, Any>()
            payload[PAYLOAD_ARG_CODE] = code
            payload[PAYLOAD_ARG_STORAGE] = init
            return payload
        }

    companion object {
        const val PAYLOAD_ARG_CODE = "code"
        const val PAYLOAD_ARG_STORAGE = "storage"
    }
}
