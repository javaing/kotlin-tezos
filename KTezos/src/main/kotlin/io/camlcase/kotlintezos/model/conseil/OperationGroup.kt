/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.conseil

import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.kotlintezos.model.operation.OperationType

/**
 * Information about an operation in the blockchain
 *
 * @param hash The operation hash
 */
data class OperationGroup(
    val chainId: String,
    val hash: String,
    val branch: String,
    val signature: String,
    val blockId: String,
    val details: List<OperationDetail>
)

/**
 * Conseil representation of an operation in the blockchain pertaining to an operation group
 * (Different than a [ConseilOperation], fields are different!)
 *
 * @param errors List of possible blockchain errors
 * (E.g.: [proto.006-PsCARTHA.michelson_v1.runtime_error,proto.006-PsCARTHA.michelson_v1.script_rejected] )
 * @param originatedContract If the operation is [OperationType.ORIGINATION], the originated contract KT1 address
 */
data class OperationDetail(
    val type: OperationType,
    val status: OperationResultStatus,
    val errors: List<String>?,
    val originatedContract: Address?
)
