/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.conseil

import io.camlcase.kotlintezos.data.conseil.ConseilPredicateType
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.fees.OperationFees


/**
 * An operation retrieved from Conseil.
 *
 * @param kind Of operation (E.g.: "transaction")
 * @param timestamp Date in number of seconds since the Unix Epoch on January 1st, 1970 at UTC
 * @param operationGroupHash The operation ID hash of the operation
 */
interface ConseilOperation {
    val source: Address
    val kind: OperationType
    val status: ConseilPredicateType.OperationStatus
    val timestamp: Long

    val fees: OperationFees?
    val consumedGas: Int

    val blockHash: String
    val blockLevel: Int
    val operationGroupHash: String
    val operationId: Int

    val smartContractInfo: ConseilSmartContractInfo?
}

/**
 * @param parameters Michelson parameters called within the operation
 */
data class ConseilSmartContractInfo(
    val parameters: String,
    val parametersMicheline: String,
    val entrypoint: String
)

