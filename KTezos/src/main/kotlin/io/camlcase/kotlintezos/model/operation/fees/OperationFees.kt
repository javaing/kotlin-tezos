/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.model.operation.fees

import io.camlcase.kotlintezos.model.RPCErrorResponse
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.operation.fees.Maximums
import kotlin.math.absoluteValue

data class OperationFees(
    val fee: Tez,
    val gasLimit: Int,
    val storageLimit: Int,
    val extraFees: ExtraFees? = null
) {

    operator fun plus(other: OperationFees): OperationFees {
        val plusExtraFees = when {
            this.extraFees == null -> other.extraFees
            other.extraFees == null -> this.extraFees
            else -> this.extraFees + other.extraFees
        }
        return OperationFees(
            this.fee + other.fee,
            this.gasLimit.absoluteValue + other.gasLimit.absoluteValue,
            this.storageLimit.absoluteValue + other.storageLimit.absoluteValue,
            plusExtraFees
        )

    }

    companion object {
        /**
         * 0 XTZ for dry-run but max gas and storage values to ensure it succeeds.
         */
        val simulationFees = OperationFees(
            Tez.zero,
            Maximums.GAS,
            Maximums.STORAGE
        )

        val empty = OperationFees(
            Tez.zero,
            0,
            0
        )
    }
}

/**
 * Wraps several [OperationFees] and an accumulated value of all
 */
data class CalculatedFees(
    val operationsFees: List<OperationFees>,
    val accumulatedFees: OperationFees,
    val internalErrors: List<RPCErrorResponse>? = null
) {
    constructor(operationsFees: List<OperationFees>) : this(
        operationsFees,
        calculateAccumulated(operationsFees)
    )

    companion object {
        fun calculateAccumulated(operationsFees: List<OperationFees>): OperationFees {
            var accumulatedFees = OperationFees.empty
            operationsFees.forEach {
                accumulatedFees += it
            }
            return accumulatedFees
        }
    }
}
