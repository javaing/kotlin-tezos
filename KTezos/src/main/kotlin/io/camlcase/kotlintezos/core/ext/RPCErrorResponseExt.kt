/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.core.ext

import io.camlcase.kotlintezos.model.RPCErrorCause
import io.camlcase.kotlintezos.model.RPCErrorResponse
import io.camlcase.kotlintezos.model.TezosError

/**
 * Extract real reason of an error from the "Failed to parse the request body: No case matched" block of text.
 *
 * @return strippedCause or null if it was not an Unhandled error
 */
fun RPCErrorResponse?.stripUnhandledErrorCause(): String? {
    return this?.let {
        val unhandledMessage = listOf("Unhandled error (Failure \"", "\")")
        if (it.cause.contains(unhandledMessage[0])) {
            val split = it.cause.split(unhandledMessage[0], unhandledMessage[1])
            if (split.size == 3 && split[1].isNotBlank()) {
                return split[1]
            }
        }
        return null
    }
}

/**
 * Given a list of causes, see if it matches [causeType]
 * Use it with [listCauses]
 */
fun List<String>?.isTypeOfError(causeType: RPCErrorCause): Boolean {
    this?.apply {
        for (message in this) {
            for (value in causeType.values) {
                if (message.contains(value)) {
                    return true
                }
            }
        }
    }
    return false
}

/**
 * Loops through the errors returned by the RPC to see if it matches [causeType]
 */
fun Throwable?.isTypeOfError(causeType: RPCErrorCause): Boolean {
    this?.apply {
        if (this is TezosError && !this.rpcErrors.isNullOrEmpty()) {
            val causes = this.rpcErrors.listCauses()
            return causes.isTypeOfError(causeType)
        }
    }
    return false
}

/**
 * Lists causes of a [TezosError].
 */
fun List<RPCErrorResponse>?.listCauses(): List<String> {
    val list = ArrayList<String>()
    if (this != null) {
        for (error in this) {
            val cause = error.stripUnhandledErrorCause() ?: error.cause
            list.add(cause)
            error.message?.let {
                list.add(it)
            }
        }
    }
    return list
}
