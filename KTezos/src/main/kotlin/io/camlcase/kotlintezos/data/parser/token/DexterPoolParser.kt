/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.data.parser.token

import io.camlcase.kotlintezos.core.ext.toJson
import io.camlcase.kotlintezos.data.Parser
import io.camlcase.kotlintezos.data.dexter.DexterPool
import io.camlcase.kotlintezos.data.parser.operation.MichelsonParameterParser
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.smartcontract.michelson.BigIntegerMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.MichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.MultiPairMichelsonParameter
import io.camlcase.kotlintezos.smartcontract.michelson.PairMichelsonParameter
import java.math.BigInteger

/**
 * Extracts a Dexter pool value from the storage data. It's implemented specifically for Dexter contracts and
 * will only work if the storage is parametrised in a certain way. If it doesn't find the argument, it will return zero
 */
@Suppress("TooGenericExceptionCaught", "MagicNumber")
class DexterPoolParser : Parser<DexterPool> {
    override fun parse(jsonData: String): DexterPool? {
        return parseString(jsonData)
    }

    fun parse(map: Map<String, Any?>): DexterPool {
        return try {
            parseString(map.toJson())
        } catch (e: Exception) {
            DexterPool.empty
        }
    }

    private fun parseString(data: String?): DexterPool {
        return data?.let {
            try {
                val michelsonParameter = MichelsonParameterParser().parse(it)
                return@let when (michelsonParameter) {
                    is MultiPairMichelsonParameter -> {
                        parseEdoStorage(michelsonParameter)
                    }
                    else -> {
                        parseStorage(michelsonParameter)
                    }
                }
            } catch (e: Exception) {
                // If we pass anything invalid, just return empty
                DexterPool.empty
            }
        } ?: DexterPool.empty
    }

    private fun parseEdoStorage(parameter: MultiPairMichelsonParameter): DexterPool {
        /**
         * EDO
         * pair big_map_id
         *      (pair (bool :calledByDexter) (bool :freezeBaker) (nat :lqtTotal))
         *      (pair (address :manager) (address :tokenAddress))
         *      (nat :tokenPool)
         *      (mutez :xtzPool)
         */
        return (parameter as? MultiPairMichelsonParameter)?.args?.let { args ->
            if (args.size > 4) {
                val tokenPool =
                    (args[3] as? BigIntegerMichelsonParameter)?.value
                        ?: BigInteger.ZERO
                val xtzPool =
                    (args[4] as? BigIntegerMichelsonParameter)?.value
                        ?: BigInteger.ZERO
                return@let DexterPool(Tez(xtzPool.toString()), tokenPool)
            }
            DexterPool.empty
        } ?: DexterPool.empty
    }

    private fun parseStorage(parameter: MichelsonParameter?): DexterPool {
        return ((((parameter as? PairMichelsonParameter)
            ?.right as? PairMichelsonParameter)
            ?.right as? PairMichelsonParameter)
            ?.right as? PairMichelsonParameter)?.let { pair ->
            val tokenPool =
                (pair.left as? BigIntegerMichelsonParameter)?.value
                    ?: BigInteger.ZERO
            val xtzPool =
                (pair.right as? BigIntegerMichelsonParameter)?.value
                    ?: BigInteger.ZERO
            return@let DexterPool(Tez(xtzPool.toString()), tokenPool)
        } ?: DexterPool.empty
    }

    companion object {
        const val ARGS = "args"
    }
}
