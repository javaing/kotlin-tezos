/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.data.blockchain

import io.camlcase.kotlintezos.data.RPC
import io.camlcase.kotlintezos.data.parser.IntParser
import io.camlcase.kotlintezos.data.parser.StringMapParser
import io.camlcase.kotlintezos.data.parser.StringParser
import io.camlcase.kotlintezos.model.Address

const val RPC_GET_HEAD_PATH = "/chains/main/blocks/head"
const val RPC_GET_HEAD_HASH_PATH = "/chains/main/blocks/head/hash"
const val RPC_PATH_CONTRACTS = "/chains/main/blocks/head/context/contracts/"
const val RPC_PATH_SUFFIX_COUNTER = "/counter"
const val RPC_PATH_SUFFIX_MANAGER_KEY = "/manager_key"

/**
 * An RPC which will retrieve a JSON map of info about the head of the current chain.
 */
internal class GetChainHeadRPC : RPC<Map<String, Any?>>(endpoint = RPC_GET_HEAD_PATH, parser = StringMapParser())

/**
 * An RPC which will retrieve the hash of the head of the main chain.
 */
internal class GetChainHeadHashRPC : RPC<String>(endpoint = RPC_GET_HEAD_HASH_PATH, parser = StringParser())

/**
 * A RPC which will retrieve the counter for an address.
 */
internal class GetAddressCounterRPC(val address: Address) : RPC<Int>(
    endpoint = "$RPC_PATH_CONTRACTS$address$RPC_PATH_SUFFIX_COUNTER",
    parser = IntParser()
)

/**
 * An RPC that will retrieve the manager key of a given address.
 */
internal class GetAddressManagerKeyRPC(val address: Address) : RPC<String>(
    endpoint = "$RPC_PATH_CONTRACTS$address$RPC_PATH_SUFFIX_MANAGER_KEY",
    parser = StringParser()
)
