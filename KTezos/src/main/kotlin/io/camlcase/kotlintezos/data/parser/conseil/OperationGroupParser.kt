/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.data.parser.conseil

import io.camlcase.kotlintezos.data.Parser
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.kotlintezos.model.conseil.OperationDetail
import io.camlcase.kotlintezos.model.conseil.OperationGroup
import io.camlcase.kotlintezos.model.operation.OperationType
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonObject
import kotlinx.serialization.json.JsonPrimitive

/**
 * Parse the resulting JSON from a query to Conseil to an [OperationGroup].
 */
internal class OperationGroupParser : Parser<OperationGroup> {
    override fun parse(jsonData: String): OperationGroup? {
        try {
            val parsedMap = Json.parseToJsonElement(jsonData) as? JsonObject
            parsedMap?.let { map ->
                val contents = map["operation_group"] as JsonObject
                val chainId: String = (contents["chainId"] as JsonPrimitive).content
                val hash: String = (contents["hash"] as JsonPrimitive).content
                val branch: String = (contents["branch"] as JsonPrimitive).content
                val signature: String = (contents["signature"] as JsonPrimitive).content
                val blockId: String = (contents["blockId"] as JsonPrimitive).content

                val operations = map["operations"] as JsonArray
                val operationList = ArrayList<OperationDetail>()
                for (operation in operations) {
                    val operationMap = (operation as JsonObject)
                    try {
                        operationList.add(parseOperationDetail(operationMap))
                    } catch (e: Exception) {
                        // Don't add
                    }
                }

                return OperationGroup(chainId, hash, branch, signature, blockId, operationList)
            }
        } catch (e: Exception) {
            // If the JSON and the server error are nil, it means the block is not found yet.
            return null
        }
        return null
    }

    private fun parseOperationDetail(jsonObject: JsonObject): OperationDetail {
        val type = OperationType.get((jsonObject["kind"] as JsonPrimitive).content)
        val status = OperationResultStatus.get((jsonObject["status"] as JsonPrimitive).content)

        val errors: List<String>? = if (jsonObject.contains("errors")) {
            (jsonObject["errors"] as JsonPrimitive).content.replace("[", "")
                .replace("]", "")
                .split(",")
        } else {
            null
        }

        val originatedContract: Address? = if (type == OperationType.ORIGINATION) {
            (jsonObject["originatedContracts"] as JsonPrimitive).content
        } else {
            null
        }
        return OperationDetail(type, status, errors, originatedContract)
    }

}
