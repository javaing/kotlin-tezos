/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.data.parser

import io.camlcase.kotlintezos.core.ext.parseGeneralErrors
import io.camlcase.kotlintezos.core.parser.toPrimitiveMap
import io.camlcase.kotlintezos.data.Parser
import io.camlcase.kotlintezos.data.parser.operation.OperationParser
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.operation.Operation
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.payload.OperationPayload
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonObject

/**
 * Parse the resulting JSON from a run-operation to a [OperationPayload].
 *
 * IMPORTANT: It only parses operations supported by [OperationType]. If other type, it will try to parse them
 * as [TransactionOperation] and might/not return that specific one.
 */
class ParsedOperationParser : Parser<List<Operation>> {
    override fun parse(jsonData: String): List<Operation>? {
        val errors = parseGeneralErrors(jsonData)
        if (!errors.isNullOrEmpty()) {
            throw TezosError(TezosErrorType.RPC_ERROR, errors)
        }

        val operations = ArrayList<Operation>()
        val operationParser = OperationParser()
        val list = Json.parseToJsonElement(jsonData) as JsonArray
        for (item in list) {
            (item as? JsonObject)?.let { map ->
                try {
                    val contents = map["contents"] as JsonArray
                    for (content in contents) {
                        val contentMap = (content as JsonObject).toPrimitiveMap()
                        operationParser.parse(contentMap)?.apply {
                            operations.add(this)
                        }
                    }

                } catch (e: Exception) {
                    return null
                }
            } ?: return null
        }
        return operations
    }

}

/**
 * Parses the json as list of maps to return the first element.
 */
class ParsedOperationPayloadParser : Parser<Map<String, Any?>> {
    override fun parse(jsonData: String): Map<String, Any?>? {
        val errors = parseGeneralErrors(jsonData)
        if (!errors.isNullOrEmpty()) {
            throw TezosError(TezosErrorType.RPC_ERROR, errors)
        }

        val list = Json.parseToJsonElement(jsonData) as? JsonArray
        if (!list.isNullOrEmpty()) {
            (list[0] as? JsonObject)?.let { map ->
                return map.toPrimitiveMap()
            }
        }

        return null
    }

}
