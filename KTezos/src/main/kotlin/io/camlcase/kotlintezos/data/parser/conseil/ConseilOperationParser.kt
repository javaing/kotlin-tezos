/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.data.parser.conseil

import io.camlcase.kotlintezos.core.parser.toPrimitiveMap
import io.camlcase.kotlintezos.data.Parser
import io.camlcase.kotlintezos.data.conseil.ConseilPredicateType
import io.camlcase.kotlintezos.data.parser.StringMapParser
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.conseil.ConseilDelegation
import io.camlcase.kotlintezos.model.conseil.ConseilOperation
import io.camlcase.kotlintezos.model.conseil.ConseilOrigination
import io.camlcase.kotlintezos.model.conseil.ConseilReveal
import io.camlcase.kotlintezos.model.conseil.ConseilSmartContractInfo
import io.camlcase.kotlintezos.model.conseil.ConseilTransaction
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonObject

/**
 * @see [ConseilOperation]
 */
class ConseilOperationParser : Parser<ConseilOperation> {
    /**
     * @return The operation or null if it couldn't be parsed
     */
    override fun parse(jsonData: String): ConseilOperation? {
        val parsedMap = StringMapParser().parse(jsonData)
        parsedMap?.let { map ->
            return parse(map)
        }
        return null
    }

    @Suppress("LongMethod")
    fun parse(map: Map<String, Any?>): ConseilOperation? {
        try {
            val source = map["source"] as String
            val kind = map["kind"] as String
            val timestamp: Long = map["timestamp"] as Long
            val status = map["status"] as String

            val gasLimit = map["gas_limit"] as? Int
            val storageLimit = map["storage_limit"] as? Int
            val operationFees = (map["fee"] as? Long)?.let {
                val tezFee = Tez(it.toString())
                OperationFees(tezFee, gasLimit ?: 0, storageLimit ?: 0)
            }
            val consumedGas = (map["consumed_gas"] as Long).toInt()

            val blockHash = map["block_hash"] as String
            val blockLevel = (map["block_level"] as Long).toInt()
            val groupHash = map["operation_group_hash"] as String
            val operationId = (map["operation_id"] as Long).toInt()

            // Smart Contract Info
            val parameters = map["parameters"] as? String
            val parametersMicheline = map["parameters_micheline"] as? String
            val entrypoints = map["parameters_entrypoints"] as? String
            val info =
                if (parameters != null && parametersMicheline != null && entrypoints != null) {
                    ConseilSmartContractInfo(parameters, parametersMicheline, entrypoints)
                } else {
                    null
                }

            return when (OperationType.get(kind)) {
                OperationType.TRANSACTION -> {
                    val destination = map["destination"] as String
                    val amount: Number = if (map["amount"] is Int) {
                        map["amount"] as Int
                    } else {
                        map["amount"] as Long
                    }

                    ConseilTransaction(
                        source,
                        destination,
                        ConseilPredicateType.OperationStatus.get(status),
                        Tez(amount.toString()),
                        timestamp,
                        operationFees,
                        consumedGas,
                        blockHash,
                        blockLevel,
                        groupHash,
                        operationId,
                        info
                    )
                }
                OperationType.DELEGATION -> {
                    val delegate = map["delegate"] as String
                    ConseilDelegation(
                        source,
                        delegate,
                        ConseilPredicateType.OperationStatus.get(status),
                        timestamp,
                        operationFees,
                        consumedGas,
                        blockHash,
                        blockLevel,
                        groupHash,
                        operationId,
                        info
                    )
                }
                OperationType.REVEAL -> {
                    val publicKey = map["public_key"] as String
                    ConseilReveal(
                        source,
                        publicKey,
                        ConseilPredicateType.OperationStatus.get(status),
                        timestamp,
                        operationFees,
                        consumedGas,
                        blockHash,
                        blockLevel,
                        groupHash,
                        operationId,
                        info
                    )
                }
                OperationType.ORIGINATION -> {
                    val contractAddress = map["originated_contracts"] as String
                    val contractScript = map["script"] as String
                    ConseilOrigination(
                        source,
                        contractAddress,
                        contractScript,
                        ConseilPredicateType.OperationStatus.get(status),
                        timestamp,
                        operationFees,
                        consumedGas,
                        blockHash,
                        blockLevel,
                        groupHash,
                        operationId,
                        info
                    )
                }
            }
        } catch (e: Throwable) {
            e.printStackTrace()
            return null
        }
    }
}

/**
 * Converts JSONArray to a List of [ConseilOperation]
 * @see ConseilOperationParser
 */
class ConseilOperationListParser : Parser<List<ConseilOperation>> {
    override fun parse(jsonData: String): List<ConseilOperation>? {
        val jsonArray = Json.parseToJsonElement(jsonData) as JsonArray
        val list = ArrayList<ConseilOperation>()
        val itemParser = ConseilOperationParser()
        for (map in jsonArray) {
            val parsedMap = (map as? JsonObject)?.toPrimitiveMap()
            parsedMap?.let {
                itemParser.parse(it)?.let { conseilTransaction -> list.add(conseilTransaction) }
            }
        }
        return list
    }
}
