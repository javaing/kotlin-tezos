/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.data.parser.tzkt

import io.camlcase.kotlintezos.data.Parser
import io.camlcase.kotlintezos.data.parser.StringMapParser
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.tzkt.TzKtDelegationResponse
import io.camlcase.kotlintezos.model.tzkt.TzKtOperation
import io.camlcase.kotlintezos.model.tzkt.TzKtOriginationResponse
import io.camlcase.kotlintezos.model.tzkt.TzKtRevealResponse
import io.camlcase.kotlintezos.model.tzkt.TzKtTransactionResponse
import io.camlcase.kotlintezos.model.tzkt.dto.map
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonArray
import kotlinx.serialization.json.JsonObject

/**
 * JSON to [TzKtOperation] converter. For use in [RPC]s.
 */
class TzKtOperationParser : Parser<TzKtOperation> {
    val format = Json {
        isLenient = true
        ignoreUnknownKeys = true
    }

    override fun parse(jsonData: String): TzKtOperation? {
        StringMapParser().parse(jsonData)?.apply {
            val type: OperationType = OperationType.get(this["type"] as String)
            val entity = when (type) {
                OperationType.TRANSACTION -> format.decodeFromString(TzKtTransactionResponse.serializer(), jsonData)
                OperationType.DELEGATION -> format.decodeFromString(TzKtDelegationResponse.serializer(), jsonData)
                OperationType.REVEAL -> format.decodeFromString(TzKtRevealResponse.serializer(), jsonData)
                OperationType.ORIGINATION -> format.decodeFromString(TzKtOriginationResponse.serializer(), jsonData)
            }
            return entity.map()
        }

        return null
    }

}

/**
 * JSON to [TzKtOperation] list converter. For use in [RPC]s.
 * @see TzKtOperationParser
 */
class TzKtOperationListParser : Parser<List<TzKtOperation>> {
    private val operationParser = TzKtOperationParser()

    override fun parse(jsonData: String): List<TzKtOperation>? {
        val operations = ArrayList<TzKtOperation>()
        val list = Json.parseToJsonElement(jsonData) as JsonArray
        for (item in list) {
            parseOperation(item)?.apply {
                operations.add(this)
            }
        }
        return operations
    }

    @Suppress("TooGenericExceptionCaught")
    private fun parseOperation(item: Any): TzKtOperation? {
        (item as? JsonObject)?.let { map ->
            try {
                return operationParser.parse(map.toString())
            } catch (e: Exception) {
                throw TezosError(TezosErrorType.INTERNAL_ERROR, exception = e)
            }
        }
        return null
    }
}
