/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.data.tzkt

import io.camlcase.kotlintezos.TzKtClient.Companion.SUPPORTED_VERSION
import io.camlcase.kotlintezos.data.RPC
import io.camlcase.kotlintezos.data.parser.tzkt.TzKtOperationListParser
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.tzkt.TzKtOperation

/**
 * Returns a list of operations related to the specified [source]. Currently unavailable: Native Token Receives
 *
 * [See more](https://api.tzkt.io/#operation/Accounts_GetOperations)
 *
 * @param parameters Filters for the http query
 */
class GetOperationsByAddressRPC(
    private val source: Address,
    private val parameters: Map<GetOperationsParameter, String>,
) : RPC<List<TzKtOperation>>(
    endpoint = SUPPORTED_VERSION + ENDPOINT_PATH + source + ENDPOINT_OPERATIONS,
    queryParameters = parameters.mapKeys { it.key.paramName },
    parser = TzKtOperationListParser()
) {

    companion object {
        internal const val ENDPOINT_PATH = "/accounts/"
        internal const val ENDPOINT_OPERATIONS = "/operations"
        const val QUERY_PARAMS_TYPE_VALUE = "delegation,origination,transaction,reveal"
    }
}

/**
 * Available filters for [GetOperationsByAddressRPC] call.
 */
enum class GetOperationsParameter(val paramName: String) {
    /**
     * Start of the datetime range to filter by (ISO 8601, e.g. 2019-11-31)
     */
    FROM("from"),

    /**
     * End of the datetime range to filter by (ISO 8601, e.g. 2019-12-31)
     */
    TO("to"),

    /**
     * Comma separated list of operation types to return (endorsement, ballot, proposal, activation, double_baking, double_endorsing, nonce_revelation, delegation, origination, transaction, reveal, migration, revelation_penalty, baking)
     */
    TYPE("type"),

    /**
     * Id of the last operation received, which is used as an offset for pagination.
     * Default: 0
     */
    LAST_ID("lastId"),

    /**
     * Number of items to return
     * Default: 100
     */
    LIMIT("limit"),

    /**
     * Default: 1. Sort mode (0 - ascending, 1 - descending)
     */
    SORT("sort"),

    /**
     * Comma-separated list of ticker symbols to inject historical prices into response
     * Default: "None"
     */
    QUOTE("quote"),

    /**
     * Filters operations by timestamp.
     * Specify a datetime to get items where the specified field is greater than the specified value.
     * Example: ?timestamp.gt=2020-02-20T02:40:57Z.
     */
    TIMESTAMP_GT("timestamp.gt")
}
