/**
 * # Released under MIT License
 * <p>
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.kotlintezos.test;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;

import io.camlcase.kotlintezos.TezosNodeClient;
import io.camlcase.kotlintezos.model.Tez;
import io.camlcase.kotlintezos.model.TezosCallback;
import io.camlcase.kotlintezos.model.TezosError;
import io.camlcase.kotlintezos.model.TezosErrorType;
import io.camlcase.kotlintezos.model.operation.payload.OperationPayload;
import io.camlcase.kotlintezos.operation.OperationFeesPolicy;
import io.camlcase.kotlintezos.operation.forge.ForgingVerifier;
import io.camlcase.kotlintezos.smartcontract.michelson.IntegerMichelsonParameter;
import io.camlcase.kotlintezos.smartcontract.michelson.MichelsonParameter;
import io.camlcase.kotlintezos.smartcontract.michelson.PairMichelsonParameter;
import io.camlcase.kotlintezos.smartcontract.michelson.RightMichelsonParameter;
import io.camlcase.kotlintezos.smartcontract.michelson.StringMichelsonParameter;
import io.camlcase.kotlintezos.test.network.MockServerTest;
import io.camlcase.kotlintezos.test.util.Params;
import io.camlcase.kotlintezos.test.util.RPCConstants;
import io.camlcase.kotlintezos.wallet.SDWallet;

/**
 * Integration tests for RPC POST Methods in {@link TezosNodeClient}
 */
public class TezosNodeClientOperationTest extends MockServerTest {

    public TezosNodeClient initClient() {
        ForgingVerifier verifier = new ForgingVerifier() {

            @Override
            public boolean verify(@NotNull Map<String, ?> forgedPayload, @NotNull Map<String, ?> parsedPayload) {
                return true;
            }

            @NotNull
            @Override
            public CompletableFuture<Boolean> verify(@NotNull String blockId, @NotNull String operationHash, @NotNull OperationPayload payload) {
                CompletableFuture<Boolean> future = new CompletableFuture<>();
                future.complete(true);
                return future;
            }
        };
        return io.camlcase.kotlintezos.test.TezosNodeClientTest.initClient(getMockClient(), verifier);
    }

    private static List<String> MNEMONIC = new ArrayList<>(Arrays.asList(
            "soccer",
            "click",
            "number",
            "muscle",
            "police",
            "corn",
            "couch",
            "bitter",
            "gorilla",
            "camp",
            "camera",
            "shove",
            "expire",
            "praise",
            "pill"));

    @Test
    public void testSendTransactionOK() throws InterruptedException {
        callMockDispatcher();
        SDWallet wallet = SDWallet.Companion.invoke(MNEMONIC, "");
        Tez tez = Tez.Companion.invoke(1.0);
        TezosNodeClient client = initClient();

        TezosCallback<String> callback = new TezosCallback<String>() {
            @Override
            public void onSuccess(@Nullable String item) {
                System.out.println("*** CALLBACK onSuccess " + item);
                Assert.assertNotNull(item);
                Assert.assertEquals(RPCConstants.INJECT_RESULT, item);
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(@NotNull TezosError error) {
                Assert.fail("Should not call onFailure");
                countDownLatch.countDown();
            }
        };
        assert wallet != null;
        client.send(tez, Params.fakeAddress, wallet.getMainAddress(), wallet, new OperationFeesPolicy.Default(), callback);
        countDownLatch.await();
    }

    @Test
    public void testSendTransactionKO() throws InterruptedException {
        callErrorDispatcher();
        SDWallet wallet = SDWallet.Companion.invoke(MNEMONIC, "");
        Tez tez = Tez.Companion.invoke(1.0);
        TezosNodeClient client = initClient();

        TezosCallback<String> callback = new TezosCallback<String>() {
            @Override
            public void onSuccess(@Nullable String item) {
                Assert.fail("KO Calls should not call onSuccess");
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(@NotNull TezosError error) {
                System.out.println("*** CALLBACK onFailure " + error);
                Assert.assertNotNull(error);
                Assert.assertSame(error.getType(), TezosErrorType.RPC_ERROR);
                countDownLatch.countDown();
            }
        };
        assert wallet != null;
        client.send(tez, Params.fakeAddress, wallet.getMainAddress(), wallet, new OperationFeesPolicy.Default(), callback);
        countDownLatch.await();
    }


    @Test
    public void testDelegateOK() throws InterruptedException {
        callMockDispatcher();
        SDWallet wallet = SDWallet.Companion.invoke(MNEMONIC, "");
        TezosNodeClient client = initClient();

        TezosCallback<String> callback = new TezosCallback<String>() {
            @Override
            public void onSuccess(@Nullable String item) {
                System.out.println("*** CALLBACK onSuccess " + item);
                Assert.assertNotNull(item);
                Assert.assertEquals(RPCConstants.INJECT_RESULT, item);
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(@NotNull TezosError error) {
                Assert.fail("Should not call onFailure");
                countDownLatch.countDown();
            }
        };
        assert wallet != null;
        client.delegate(wallet.getMainAddress(), Params.fakeAddress, wallet, new OperationFeesPolicy.Default(), callback);
        countDownLatch.await();
    }

    @Test
    public void testDelegateKO() throws InterruptedException {
        callErrorDispatcher();
        SDWallet wallet = SDWallet.Companion.invoke(MNEMONIC, "");
        TezosNodeClient client = initClient();

        TezosCallback<String> callback = new TezosCallback<String>() {
            @Override
            public void onSuccess(@Nullable String item) {
                Assert.fail("KO Calls should not call onSuccess");
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(@NotNull TezosError error) {
                System.out.println("*** CALLBACK onFailure " + error);
                Assert.assertNotNull(error);
                Assert.assertSame(error.getType(), TezosErrorType.RPC_ERROR);
                countDownLatch.countDown();
            }
        };
        assert wallet != null;
        client.delegate(wallet.getMainAddress(), Params.fakeAddress, wallet, new OperationFeesPolicy.Default(), callback);
        countDownLatch.await();
    }

    @Test
    public void testCallSmartContractOK() throws InterruptedException {
        callMockDispatcher();
        SDWallet wallet = SDWallet.Companion.invoke(MNEMONIC, "");

        MichelsonParameter michelson = new PairMichelsonParameter(
                new StringMichelsonParameter("test", null),
                new RightMichelsonParameter(
                        new IntegerMichelsonParameter(10, null),
                        null
                ),
                null
        );
        TezosNodeClient client = initClient();
        TezosCallback<String> callback = new TezosCallback<String>() {
            @Override
            public void onSuccess(@Nullable String item) {
                System.out.println("*** CALLBACK onSuccess " + item);
                Assert.assertNotNull(item);
                Assert.assertEquals(RPCConstants.INJECT_RESULT, item);
                countDownLatch.countDown();
            }

            @Override
            public void onFailure(@NotNull TezosError error) {
                Assert.fail("Should not call onFailure");
                countDownLatch.countDown();
            }
        };
        assert wallet != null;
        client.call(wallet.getMainAddress(), "KT1TEST", Tez.Companion.getZero(), michelson, null, wallet, new OperationFeesPolicy.Default(), callback);
        countDownLatch.await();
    }
}
