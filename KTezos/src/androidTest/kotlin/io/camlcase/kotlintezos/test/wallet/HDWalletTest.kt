/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.wallet

import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.validTezosAddress
import io.camlcase.kotlintezos.test.util.Wallet
import io.camlcase.kotlintezos.wallet.HDWallet
import io.camlcase.kotlintezos.wallet.SDWallet
import io.camlcase.kotlintezos.wallet.crypto.EncoderFacade
import io.camlcase.kotlintezos.wallet.crypto.toHexString
import org.junit.Assert
import org.junit.Test

/**
 * Test class for [HDWallet]
 */
class HDWalletTest {

    @Test
    fun test_DeterministicAddress() {
        val wallet = HDWallet(testMnemonic, testPassword)
        val sdWallet = SDWallet(testMnemonic, testPassword)
        Assert.assertEquals(sdWallet!!.mainAddress, wallet.linearAddress)
    }

    // Check Wallet-core test also passes with KotlinTezos implementation
    @Test
    fun test_PublicKey() {
        val wallet = HDWallet(testMnemonic, testPassword)
        val expectedAddress = "tz1RsC3AREfrMwh6Hdu7qGKxBwb1VgwJv1qw"
        val expectedPublickKeyHash =
            "c834147f97bcf95bf01f234455646a197f70b25e93089591ffde8122370ad371"
        Assert.assertEquals(expectedAddress, wallet.mainAddress)
        Assert.assertEquals(expectedPublickKeyHash, wallet.publicKey.bytes.toHexString())
    }

    @Test
    fun test_DefaultWallet() {
        val wallet = HDWallet()
        Assert.assertTrue(wallet.mainAddress.validTezosAddress())
        Assert.assertTrue(wallet.mnemonic.size == 12)
    }

    @Test
    fun test_Accounts_DefaultDerivationPath() {
        val expectedAddress0 = "tz1RsC3AREfrMwh6Hdu7qGKxBwb1VgwJv1qw"
        val expectedAddress1 = "tz1h2aWgnHvdiZSRD9epvtG3xt5DAn1nhoYE"
        val expectedAddress2 = "tz1PXRnbWscMpfuPMaCzEL2SYNPKUSyzbWQz"
        val wallet = HDWallet(testMnemonic, testPassword)

        Assert.assertNotNull(wallet)
        Assert.assertTrue(wallet.mainAddress == expectedAddress0)
        Assert.assertTrue(wallet.address(1) == expectedAddress1)
        Assert.assertTrue(wallet.address(2) == expectedAddress2)
    }

    @Test(expected = TezosError::class)
    fun test_InvalidDerivationPath() {
        val wallet = HDWallet(testMnemonic, testPassword, "m/44'/18'/0/0")
    }

    @Test(expected = TezosError::class)
    fun test_InvalidDerivationPath_Accounts() {
        val wallet = HDWallet(testMnemonic, testPassword)
        wallet.address("m/44'/18'/0/0")
    }

    @Test(expected = TezosError::class)
    fun test_ShortDerivationPath() {
        val wallet = HDWallet(testMnemonic, testPassword, "m/44'/1729'/0&$")
    }

    @Test
    fun test_Sign() {
        val bytesToSign =
            "ab64bda0a3d3416dd82a81646cd6905a3b949c492d39f10f38441eef9d3a10836c0089c7b0fbb4d55dbcbd5b80c9e31edc4924bb8352840aaad32fc350810280dac40900001d36a98bc0fdfd5ef9f4a717d07a035a5e7e440800"
        val expectedEncodedSignatureSd =
            "edsigtgQtEnQ5RqyAMVf2yRKfwGDByMQNWQgvjEh9j2QSZkgtvUohgogMgTzJ4x4cxYUWQ2JnmzY31CjE1BE38kCqfbqm3BbyHG"
        val expectedEncodedSignatureHd =
            "edsigty9w3f1LsgwoZDa8pPUJZbR4DmTuqSYjsbyHHGDKhuWhWSCsLW1RrbFxGCWzkKgVDQxNhsQUGgBxvxVEh3JXQRcWcexrCB"

        val wallet = HDWallet(testMnemonic, testPassword)
        val sdWallet = SDWallet(testMnemonic, testPassword)

        val sdSignature = sdWallet!!.sign(bytesToSign)
        val hdSignature = wallet.sign(bytesToSign)

        val encodedSd = EncoderFacade.encodeSignature(sdSignature!!)
        val encodedHd = EncoderFacade.encodeSignature(hdSignature!!)

        Assert.assertEquals(expectedEncodedSignatureHd, encodedHd)
        Assert.assertEquals(expectedEncodedSignatureSd, encodedSd)
    }

    @Test
    fun test_Generate_Common() {
        var wallet = HDWallet(Wallet.mnemonic)
        Assert.assertNotNull(wallet)
        Assert.assertTrue(wallet.mainAddress == Wallet.HDWallet.address)
        Assert.assertTrue(wallet.secretKey.bytes.toHexString() == Wallet.HDWallet.privateKey)
        Assert.assertTrue(wallet.publicKey.bytes.toHexString() == Wallet.HDWallet.publicKey)

        wallet = HDWallet(Wallet.mnemonic, Wallet.passphrase)
        Assert.assertNotNull(wallet)
        Assert.assertTrue(wallet.mainAddress == Wallet.HDWallet_Pwd.address)
        Assert.assertTrue(wallet.secretKey.bytes.toHexString() == Wallet.HDWallet_Pwd.privateKey)
        Assert.assertTrue(wallet.publicKey.bytes.toHexString() == Wallet.HDWallet_Pwd.publicKey)
    }

    @Test
    fun test_Generate_Common_Path() {
        var wallet = HDWallet(Wallet.mnemonic, Wallet.passphrase, Wallet.HDWallet_Pwd_Hardened_Path.derivationPath)
        Assert.assertNotNull(wallet)
        Assert.assertTrue(wallet.mainAddress == Wallet.HDWallet_Pwd_Hardened_Path.address)
        Assert.assertTrue(wallet.secretKey.bytes.toHexString() == Wallet.HDWallet_Pwd_Hardened_Path.privateKey)
        Assert.assertTrue(wallet.publicKey.bytes.toHexString() == Wallet.HDWallet_Pwd_Hardened_Path.publicKey)

        wallet = HDWallet(Wallet.mnemonic, null, Wallet.HDWallet_Non_Hardened.derivationPath)
        Assert.assertNotNull(wallet)
        Assert.assertTrue(wallet.mainAddress == Wallet.HDWallet_Non_Hardened.address)
        Assert.assertTrue(wallet.secretKey.bytes.toHexString() == Wallet.HDWallet_Non_Hardened.privateKey)
        Assert.assertTrue(wallet.publicKey.bytes.toHexString() == Wallet.HDWallet_Non_Hardened.publicKey)
    }

    @Test
    fun test_Common_Sign() {
        val wallet = HDWallet(Wallet.mnemonic)
        val messageHex = Wallet.messageToSign.toByteArray().toHexString()
        val result = wallet!!.sign(messageHex)
        Assert.assertTrue(result!!.toHexString() == Wallet.HDWallet.signedData)
    }

    companion object {
        private val testMnemonic =
            "ripple scissors kick mammal hire column oak again sun offer wealth tomorrow wagon turn fatal"
                .split(" ")
        private const val testPassword = "TREZOR"
    }


}
