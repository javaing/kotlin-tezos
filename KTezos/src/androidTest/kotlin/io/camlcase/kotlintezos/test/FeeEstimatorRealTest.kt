/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 */
package io.camlcase.kotlintezos.test

import io.camlcase.kotlintezos.core.parser.toPrimitive
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.SmartContractScript
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.operation.OriginationOperation
import io.camlcase.kotlintezos.model.operation.TransactionOperation
import io.camlcase.kotlintezos.model.operation.fees.BurnFee
import io.camlcase.kotlintezos.model.operation.fees.CalculatedFees
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.operation.OperationFeesPolicy
import io.camlcase.kotlintezos.test.network.tezosNodeClient
import io.camlcase.kotlintezos.wallet.SDWallet
import kotlinx.serialization.json.Json
import org.junit.Assert
import org.junit.Test
import java.util.concurrent.CountDownLatch

class FeeEstimatorRealTest {
    private val client = tezosNodeClient()
    private val feesPolicy = OperationFeesPolicy.Estimate()

    @Test
    fun testEstimate_SingleOp_Transaction() {
        val wallet: SDWallet = testWallet1!!
        val to: Address = testWallet2!!.mainAddress
        val operation = TransactionOperation(
            Tez(10.0),
            wallet.mainAddress,
            to,
            OperationFees.simulationFees
        )
        val countDownLatch = CountDownLatch(1)
        client.calculateFees(
            listOf(operation),
            wallet.mainAddress,
            wallet,
            feesPolicy,
            object : TezosCallback<CalculatedFees> {
                override fun onSuccess(item: CalculatedFees?) {
                    println("** SUCCESS! $item")
                    Assert.assertNotNull(item)
                    // For instance, a single transaction to an existing implicit address will require a transaction fee
                    // of at least 0.001 273ꜩ to be included by bakers who choose to follow the default settings.
                    Assert.assertTrue(item!!.accumulatedFees.fee > Tez("400"))
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.fail("Should not call onFailure")
                    countDownLatch.countDown()
                }
            })
        countDownLatch.await()
    }

    @Test
    fun testEstimate_SingleOp_Origination() {
        val wallet: SDWallet = testWallet1!!

        val mappedCodeMicheline: List<Map<String, Any?>> = Json.parseToJsonElement(contractCode).toPrimitive() as List<Map<String, Any?>>
        val mappedInitMicheline: Map<String, Any> = mapOf(Pair("string", "hello"))
        val script = SmartContractScript(
            mappedCodeMicheline,
            mappedInitMicheline
        )

        val operation = OriginationOperation(
            wallet.mainAddress,
            OperationFees.simulationFees,
            script
        )

        val countDownLatch = CountDownLatch(1)
        client.calculateFees(
            listOf(operation),
            wallet.mainAddress,
            wallet,
            feesPolicy,
            object : TezosCallback<CalculatedFees> {
                override fun onSuccess(item: CalculatedFees?) {
                    println("** SUCCESS! $item")
                    Assert.assertNotNull(item)
                    Assert.assertTrue(item!!.accumulatedFees.fee > Tez("500"))
                    Assert.assertTrue(item!!.accumulatedFees.gasLimit > 1000)
                    // There must be storage limit when originating
                    Assert.assertTrue(item!!.accumulatedFees.storageLimit > 0)
                    Assert.assertTrue(!item!!.accumulatedFees.extraFees!!.isEmpty())
                    Assert.assertTrue(item!!.accumulatedFees.extraFees!!.asList[0] is BurnFee)
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.fail("Should not call onFailure")
                    countDownLatch.countDown()
                }
            })
        countDownLatch.await()

    }

    companion object {
        /**
         * https://tezos.gitlab.io/introduction/howtouse.html#originated-accounts-and-contracts
         */
        private val contractCode =
            "[{\"prim\":\"parameter\",\"args\":[{\"prim\":\"string\"}]},{\"prim\":\"storage\",\"args\":[{\"prim\":\"string\"}]},{\"prim\":\"code\",\"args\":[[{\"prim\":\"CAR\"},{\"prim\":\"NIL\",\"args\":[{\"prim\":\"operation\"}]},{\"prim\":\"PAIR\"}]]}]"

    }
}


