/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 */
package io.camlcase.kotlintezos.test

import io.camlcase.kotlintezos.TezosNodeClient
import io.camlcase.kotlintezos.exchange.DexterTokenClient
import io.camlcase.kotlintezos.exchange.IndexterService
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.network.DefaultNetworkClient
import io.camlcase.kotlintezos.operation.OperationFeesPolicy
import io.camlcase.kotlintezos.test.network.CamlCredentials
import io.camlcase.kotlintezos.test.util.CurrentThreadExecutor
import io.camlcase.kotlintezos.test.util.VALIDATOR_NODE_URL
import io.camlcase.kotlintezos.wallet.SDWallet
import io.camlcase.kotlintezos.wallet.Wallet
import org.junit.Test
import java.math.BigInteger

class DexterTokenClientRealTest {

    private fun initIndexter(): IndexterService {
        val networkClient = DefaultNetworkClient(
            CamlCredentials().indexterUrl(),
            false
        )
        return IndexterService(
            networkClient,
            CurrentThreadExecutor()
        )
    }

    private fun initClient(
        nodeUrl: String = TezosNodeClient.EDONET_SMARTPY_NODE,
        tokenContractAddress: Address,
        dexterContractAddress: Address
    ): DexterTokenClient {
        val nodeClient = TezosNodeClient(nodeUrl, VALIDATOR_NODE_URL)
        return DexterTokenClient(
            nodeClient,
            TezosNetwork.EDONET,
            dexterContractAddress,
            tokenContractAddress,
            initIndexter()
        )
    }

    private fun testAddLiquidity(
        dexterContractAddress: Address,
        from: Wallet = testWallet1!!,
        amount: Tez = Tez(1.0),
        minLiquidity: BigInteger = 2000.toBigInteger(),
        maxTokens: BigInteger = minLiquidity * 2.toBigInteger(),
        tokenContractAddress: Address = TokenContractRealTest.TZBTC_MOCK_ADDRESS
    ) {
        val client = initClient(
            tokenContractAddress = tokenContractAddress,
            dexterContractAddress = dexterContractAddress
        )
        client.addLiquidity(
            from.mainAddress,
            amount,
            minLiquidity,
            maxTokens,
            feesPolicy = OperationFeesPolicy.Custom(tokenToXtzFees),
            signatureProvider = from,
            callback = object : TezosCallback<String> {
                override fun onSuccess(item: String?) {
                    println("** DEXTER SUCCESS! $item - ${Thread.currentThread()}")
                }

                override fun onFailure(error: TezosError) {
                    println("** DEXTER ERROR! $error")
                }
            })

    }

    private fun testGetExchangeBalance(
        dexterContractAddress: Address,
        tokenContractAddress: Address = TokenContractRealTest.STKR_MOCK_ADDRESS
    ) {
        val client = initClient(
            tokenContractAddress = tokenContractAddress,
            dexterContractAddress = dexterContractAddress
        )
        client.getTezPool(object : TezosCallback<Tez> {
            override fun onSuccess(item: Tez?) {
                println("** DEXTER SUCCESS! $item - ${Thread.currentThread()}")
            }

            override fun onFailure(error: TezosError) {
                println("** DEXTER ERROR! $error")
            }

        })
    }

    private fun testGetExchangeTokenBalance(
        dexterContractAddress: Address,
        tokenContractAddress: Address = TokenContractRealTest.STKR_MOCK_ADDRESS
    ) {
        val client = initClient(
            tokenContractAddress = tokenContractAddress,
            dexterContractAddress = dexterContractAddress
        )
        client.getTokenPool(object : TezosCallback<BigInteger> {
            override fun onSuccess(item: BigInteger?) {
                println("** DEXTER SUCCESS! $item - ${Thread.currentThread()}")
            }

            override fun onFailure(error: TezosError) {
                println("** DEXTER ERROR! $error")
            }
        })
    }


    private fun swapTokenToTez(
        nodeUrl: String = TezosNodeClient.EDONET_SMARTPY_NODE,
        dexterContractAddress: Address,
        tokenContractAddress: Address = TokenContractRealTest.STKR_MOCK_ADDRESS,
        wallet: SDWallet = testWallet1!!
    ) {
        val client = initClient(
            tokenContractAddress = tokenContractAddress,
            dexterContractAddress = dexterContractAddress
        )
        client.tradeTokenForTez(
            wallet.mainAddress,
            tokenAmount = BigInteger.ONE,
            amount = Tez("1457007"),
            feesPolicy = OperationFeesPolicy.Custom(tokenToXtzFees),
            signatureProvider = wallet,
            callback = object : TezosCallback<String> {
                override fun onSuccess(item: String?) {
                    println("** DEXTER SUCCESS! $item - ${Thread.currentThread()}")
                }

                override fun onFailure(error: TezosError) {
                    println("** DEXTER ERROR! $error")
                }
            })
    }

    /**
     * Test against a real node.
     * Run with debugger.
     */
    @Test
    fun testRealNode() {

        // Let the connection return something
//        Thread.sleep(15 * 60 * 1000)
    }

    companion object {
        private const val TOKEN_TO_XTZ_MIN_GAS = 611_600
        private val tokenToXtzFee = Tez("61558")
        private val tokenToXtzFees = OperationFees(tokenToXtzFee, TOKEN_TO_XTZ_MIN_GAS, 257)
    }
}
