/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.kotlintezos.test.wallet

import io.camlcase.kotlintezos.wallet.SecretKey
import org.junit.Assert
import org.junit.Test

internal class SecretKeyTest {
    @Test
    fun testInvalidSeedString() {
        val invalidSeedString = "abcdefghijklmnopqrstuvwxyz"
        val key = SecretKey(invalidSeedString)
        Assert.assertNull(key)
    }
}
