/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test

import io.camlcase.kotlintezos.BCDClient
import io.camlcase.kotlintezos.model.Address
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosNetwork
import io.camlcase.kotlintezos.model.bcd.BCDAccount
import io.camlcase.kotlintezos.network.DefaultNetworkClient
import io.camlcase.kotlintezos.test.util.CurrentThreadExecutor
import org.junit.Test

/**
 * Helper class. Shouldn't be included in tests, use is for development testing.
 */
class BCDClientRealTest {

    private val client =
        BCDClient(DefaultNetworkClient(BCDClient.BETTER_CALL_DEV_URL, true), CurrentThreadExecutor())

    private fun testBalances(
        address: Address = testWallet1!!.mainAddress,
        network: TezosNetwork = TezosNetwork.DELPHINET
    ) {
        client.getAccountBalance(
            address,
            network,
            object : TezosCallback<BCDAccount> {
                override fun onSuccess(item: BCDAccount?) {
                    println("** SUCCESS! $item")
                }

                override fun onFailure(error: TezosError) {
                    println("** ERROR! $error")
                }

            }
        )
    }

    /**
     * Test against a real node.
     * Run with debugger.
     */
    @Test
    fun testRealNode() {

        // Let the connection return something
//        Thread.sleep(15 * 60 * 1000)
    }
}
