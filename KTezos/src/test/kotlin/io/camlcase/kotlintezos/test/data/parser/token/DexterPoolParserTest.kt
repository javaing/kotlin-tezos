package io.camlcase.kotlintezos.test.data.parser.token

import io.camlcase.kotlintezos.data.dexter.DexterPool
import io.camlcase.kotlintezos.data.parser.token.DexterPoolParser
import io.camlcase.kotlintezos.model.Tez
import kotlinx.serialization.json.Json
import org.junit.Assert
import org.junit.Test
import java.math.BigInteger

class DexterPoolParserTest {
    @Test
    fun testEmptyMap() {
        val input = emptyMap<String, Any>()
        val expected = DexterPool.empty
        val result = DexterPoolParser().parse(input)
        Assert.assertEquals(expected, result)
    }

    @Test
    fun testArgsNotList() {
        val input = HashMap<String, Any>()
        input[DexterPoolParser.ARGS] = "Failed"
        val expected = DexterPool.empty
        val result = DexterPoolParser().parse(input)
        Assert.assertEquals(expected, result)
    }

    @Test
    fun testArgsListOneItem() {
        val input = HashMap<String, Any>()
        input[DexterPoolParser.ARGS] = listOf("")
        val expected = DexterPool.empty
        val result = DexterPoolParser().parse(input)
        Assert.assertEquals(expected, result)
    }

    @Test
    fun testArgsList() {
        val input = HashMap<String, Any>()
        input[DexterPoolParser.ARGS] =
            Json.parseToJsonElement("[{\"int\":\"908088765443332\"},{}]")
        val expected = DexterPool.empty
        val result = DexterPoolParser().parse(input)
        Assert.assertEquals(expected, result)
    }

    @Test
    fun test_RandomStorage() {
        val input =
            "{\"prim\":\"Pair\",\"args\":[{\"int\":\"31\"},{\"prim\":\"Pair\",\"args\":[[{\"prim\":\"DUP\"},{\"prim\":\"CAR\"},{\"prim\":\"DIP\",\"args\":[[{\"prim\":\"CDR\"}]]},{\"prim\":\"DUP\"},{\"prim\":\"DUP\"},{\"prim\":\"CAR\"},{\"prim\":\"DIP\",\"args\":[[{\"prim\":\"CDR\"}]]},{\"prim\":\"DIP\",\"args\":[[{\"prim\":\"DIP\",\"args\":[{\"int\":\"2\"},[{\"prim\":\"DUP\"}]]},{\"prim\":\"DIG\",\"args\":[{\"int\":\"2\"}]}]]},{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"string\"},{\"string\":\"code\"}]},{\"prim\":\"PAIR\"},{\"prim\":\"PACK\"},{\"prim\":\"GET\"},{\"prim\":\"IF_NONE\",\"args\":[[{\"prim\":\"NONE\",\"args\":[{\"prim\":\"lambda\",\"args\":[{\"prim\":\"pair\",\"args\":[{\"prim\":\"bytes\"},{\"prim\":\"big_map\",\"args\":[{\"prim\":\"bytes\"},{\"prim\":\"bytes\"}]}]},{\"prim\":\"pair\",\"args\":[{\"prim\":\"list\",\"args\":[{\"prim\":\"operation\"}]},{\"prim\":\"big_map\",\"args\":[{\"prim\":\"bytes\"},{\"prim\":\"bytes\"}]}]}]}]}],[{\"prim\":\"UNPACK\",\"args\":[{\"prim\":\"lambda\",\"args\":[{\"prim\":\"pair\",\"args\":[{\"prim\":\"bytes\"},{\"prim\":\"big_map\",\"args\":[{\"prim\":\"bytes\"},{\"prim\":\"bytes\"}]}]},{\"prim\":\"pair\",\"args\":[{\"prim\":\"list\",\"args\":[{\"prim\":\"operation\"}]},{\"prim\":\"big_map\",\"args\":[{\"prim\":\"bytes\"},{\"prim\":\"bytes\"}]}]}]}]},{\"prim\":\"IF_NONE\",\"args\":[[{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"string\"},{\"string\":\"UStore: failed to unpack code\"}]},{\"prim\":\"FAILWITH\"}],[]]},{\"prim\":\"SOME\"}]]},{\"prim\":\"IF_NONE\",\"args\":[[{\"prim\":\"DROP\"},{\"prim\":\"DIP\",\"args\":[[{\"prim\":\"DUP\"},{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"bytes\"},{\"bytes\":\"05010000000866616c6c6261636b\"}]},{\"prim\":\"GET\"},{\"prim\":\"IF_NONE\",\"args\":[[{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"string\"},{\"string\":\"UStore: no field fallback\"}]},{\"prim\":\"FAILWITH\"}],[]]},{\"prim\":\"UNPACK\",\"args\":[{\"prim\":\"lambda\",\"args\":[{\"prim\":\"pair\",\"args\":[{\"prim\":\"pair\",\"args\":[{\"prim\":\"string\"},{\"prim\":\"bytes\"}]},{\"prim\":\"big_map\",\"args\":[{\"prim\":\"bytes\"},{\"prim\":\"bytes\"}]}]},{\"prim\":\"pair\",\"args\":[{\"prim\":\"list\",\"args\":[{\"prim\":\"operation\"}]},{\"prim\":\"big_map\",\"args\":[{\"prim\":\"bytes\"},{\"prim\":\"bytes\"}]}]}]}]},{\"prim\":\"IF_NONE\",\"args\":[[{\"prim\":\"PUSH\",\"args\":[{\"prim\":\"string\"},{\"string\":\"UStore: failed to unpack fallback\"}]},{\"prim\":\"FAILWITH\"}],[]]},{\"prim\":\"SWAP\"}]]},{\"prim\":\"PAIR\"},{\"prim\":\"EXEC\"}],[{\"prim\":\"DIP\",\"args\":[[{\"prim\":\"SWAP\"},{\"prim\":\"DROP\"},{\"prim\":\"PAIR\"}]]},{\"prim\":\"SWAP\"},{\"prim\":\"EXEC\"}]]}],{\"prim\":\"Pair\",\"args\":[{\"int\":\"1\"},{\"prim\":\"False\"}]}]}]}\n"
        val expected = DexterPool.empty
        val result = DexterPoolParser().parse(input)
        Assert.assertEquals(expected, result)
    }

    @Test
    fun test_Correct_DexterStorage_String() {
        val input =
            "{\"prim\":\"Pair\",\"args\":[{\"int\":\"123\"},{\"prim\":\"Pair\",\"args\":[{\"prim\":\"Pair\",\"args\":[{\"prim\":\"False\"},{\"prim\":\"Pair\",\"args\":[{\"prim\":\"False\"},{\"int\":\"44649450668\"}]}]},{\"prim\":\"Pair\",\"args\":[{\"prim\":\"Pair\",\"args\":[{\"string\":\"KT1B5VTw8ZSMnrjhy337CEvAm4tnT8Gu8Geu\"},{\"string\":\"KT1PWx2mnDueood7fEmfbBDKx1D9BAnnXitn\"}]},{\"prim\":\"Pair\",\"args\":[{\"int\":\"841333731\"},{\"int\":\"48982301793\"}]}]}]}]}"
        val expected = DexterPool(Tez("48982301793"), BigInteger("841333731"))
        val result = DexterPoolParser().parse(input)
        Assert.assertEquals(expected, result)
    }

    @Test
    fun test_Correct_DexterStorage_Json() {
        val input =
            "{\"prim\":\"Pair\",\"args\":[{\"int\":\"124\"},{\"prim\":\"Pair\",\"args\":[{\"prim\":\"Pair\",\"args\":[{\"prim\":\"False\"},{\"prim\":\"Pair\",\"args\":[{\"prim\":\"False\"},{\"int\":\"11995608890\"}]}]},{\"prim\":\"Pair\",\"args\":[{\"prim\":\"Pair\",\"args\":[{\"string\":\"KT1B5VTw8ZSMnrjhy337CEvAm4tnT8Gu8Geu\"},{\"string\":\"KT1LN4LPSqTMS7Sd2CJw4bbDGRkMv2t68Fy9\"}]},{\"prim\":\"Pair\",\"args\":[{\"int\":\"26427479034\"},{\"int\":\"12215326838\"}]}]}]}]}\n"
        val expected = DexterPool(Tez("12215326838"), BigInteger("26427479034"))
        val result = DexterPoolParser().parse(input)
        Assert.assertEquals(expected, result)
    }

    @Test
    fun test_DexterStorage_Edo() {
        val input =
            "{\"args\":[{\"int\":\"410\"},{\"args\":[{\"prim\":\"False\"},{\"prim\":\"False\"},{\"int\":\"0\"}],\"prim\":\"Pair\"},{\"args\":[{\"string\":\"tz1RY96NB6CWNDxy1sfqB2hXUwoCYxdw48Mm\"},{\"string\":\"KT19vLuzhGpe5F64jkxgzqrfRkSVprL4cPa5\"}],\"prim\":\"Pair\"},{\"int\":\"0\"},{\"int\":\"1234\"}],\"prim\":\"Pair\"}"
        val expected = DexterPool(Tez("1234"), BigInteger.ZERO)
        val result = DexterPoolParser().parse(input)
        Assert.assertEquals(expected, result)
    }
}
