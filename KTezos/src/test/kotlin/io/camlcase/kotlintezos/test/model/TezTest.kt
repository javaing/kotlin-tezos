/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.kotlintezos.test.model

import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosError
import org.junit.Assert
import org.junit.Test
import java.math.BigDecimal
import java.math.BigInteger
import java.math.MathContext

/**
 * Test suite for [Tez]
 */
class TezTest {

    @Test
    fun testTezCreationWith2Decimal() {
        val tez = Tez(2.07)
        Assert.assertEquals(2.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(70000.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("2070000", tez.stringRepresentation)
    }

    @Test
    fun testTezCreationWith3Decimal() {
        val tez = Tez(123.456)
        Assert.assertEquals(123.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(456000.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("123456000", tez.stringRepresentation)
    }

    @Test
    fun testTezCreationWith6Decimal() {
        val tez = Tez(123.000456)
        Assert.assertEquals(123.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(456.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("123000456", tez.stringRepresentation)
    }

    @Test
    fun testTezCreationWith6Decimal2() {
        val tez = Tez(123.456789)
        Assert.assertEquals(123.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(456789.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("123456789", tez.stringRepresentation)

    }

    @Test
    fun testTezCreationLostDecimal() {
        val tez = Tez(123.000456789)
        Assert.assertEquals(123.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(456.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("123000456", tez.stringRepresentation)
        Assert.assertEquals(123.000456.toBigDecimal(), tez.signedDecimalRepresentation)
    }

    @Test
    fun testTezCreation8LongInteger() {
        val tez = Tez(12345678.1)
        Assert.assertEquals(12345678.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(100000.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("12345678100000", tez.stringRepresentation)
        Assert.assertEquals(BigDecimal("12345678.100000", MathContext.DECIMAL128), tez.signedDecimalRepresentation)
    }

    @Test
    fun testTezCreation_Negative_Double() {
        val tez = Tez(-123.456789)
        Assert.assertEquals(123.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(456789.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("123456789", tez.stringRepresentation)
        Assert.assertEquals(123.456789.toBigDecimal(), tez.signedDecimalRepresentation)
    }

    @Test(expected = TezosError::class)
    fun testTezCreationWithEmptyString() {
        Tez("")
    }

    @Test(expected = TezosError::class)
    fun testTezCreationWithInvalidString() {
        Tez("1.90")
    }

    @Test
    fun testTezCreationWithStringSmallNumber() {
        val tez = Tez("10") // 0.000010 XTC
        Assert.assertEquals(0.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(10.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("10", tez.stringRepresentation)
        Assert.assertEquals(0.000010.toBigDecimal(), tez.signedDecimalRepresentation)
    }

    @Test
    fun testTezCreationWithStringWholeNumber() {
        val tez = Tez("3000000") // 3 XTC
        Assert.assertEquals(3.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(0.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("3000000", tez.stringRepresentation)
        Assert.assertEquals(BigDecimal("3.000000", MathContext.DECIMAL128), tez.signedDecimalRepresentation)
    }

    @Test
    fun testTezCreationWithStringWithDecimal() {
        val tez = Tez("3500000") // 3.5 XTC
        Assert.assertEquals(3.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(500000.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("3500000", tez.stringRepresentation)
        Assert.assertEquals(BigDecimal("3.500000", MathContext.DECIMAL128), tez.signedDecimalRepresentation)
    }

    @Test(expected = TezosError::class)
    fun testTezCreation_Negative_String() {
        val tez = Tez("-3500000") // 3.5 XTC
        Assert.assertEquals(3.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(500000.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("3500000", tez.stringRepresentation)
        Assert.assertEquals(3.5.toBigDecimal(), tez.signedDecimalRepresentation)
    }

    @Test
    fun testTezCreationWithStringWithVeryBigAmount() {
        val tez = Tez("1000000000000000000000") // 10^ XTC
        Assert.assertEquals(1000000000000000.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(0.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("1000000000000000000000", tez.stringRepresentation)
        Assert.assertEquals(BigDecimal("1000000000000000.000000", MathContext.DECIMAL128), tez.signedDecimalRepresentation)
    }

    @Test
    fun testTezCreationWith2BigDecimal() {
        val number = 2.07.toBigDecimal()
        val tez = Tez(number)
        Assert.assertEquals(2.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(70000.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("2070000", tez.stringRepresentation)
        Assert.assertEquals(BigDecimal("2.070000", MathContext.DECIMAL128), tez.signedDecimalRepresentation)
    }

    @Test
    fun testTezCreationWith3BigDecimal() {
        val number = 123.456.toBigDecimal()
        val tez = Tez(number)
        Assert.assertEquals(123.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(456000.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("123456000", tez.stringRepresentation)
        Assert.assertEquals(BigDecimal("123.456000", MathContext.DECIMAL128), tez.signedDecimalRepresentation)
    }

    @Test
    fun testTezCreationWith6DBigecimal() {
        val number = 123.000456.toBigDecimal()
        val tez = Tez(number)
        Assert.assertEquals(123.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(456.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("123000456", tez.stringRepresentation)
        Assert.assertEquals(number, tez.signedDecimalRepresentation)
    }

    @Test
    fun testTezCreationWith6BigDecimal2() {
        val number = 123.456789.toBigDecimal()
        val tez = Tez(number)
        Assert.assertEquals(123.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(456789.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("123456789", tez.stringRepresentation)
        Assert.assertEquals(number, tez.signedDecimalRepresentation)
    }

    @Test
    fun testTezCreationLostBigDecimal() {
        val number = 123.000456789.toBigDecimal()
        val tez = Tez(number)
        Assert.assertEquals(123.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(456.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("123000456", tez.stringRepresentation)
        Assert.assertEquals(123.000456.toBigDecimal(), tez.signedDecimalRepresentation)
    }

    @Test
    fun testTezCreation8LongIntegerBigDecimal() {
        val number = 12345678.1.toBigDecimal()
        val tez = Tez(number)
        Assert.assertEquals(12345678.toBigInteger(), tez.integerAmount)
        Assert.assertEquals(100000.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("12345678100000", tez.stringRepresentation)
        Assert.assertEquals(BigDecimal("12345678.100000", MathContext.DECIMAL128), tez.signedDecimalRepresentation)
    }

    @Test
    fun testTezCreation_Really_BigDecimal() {
        val number = BigDecimal("2975671681509007947508815.2975671681509007947508815")
        val tez = Tez(number)
        Assert.assertEquals(BigInteger("2975671681509007947508815"), tez.integerAmount)
        Assert.assertEquals(297567.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("2975671681509007947508815297567", tez.stringRepresentation)
        Assert.assertEquals(BigDecimal("2975671681509007947508815.297567"), tez.signedDecimalRepresentation)
    }

    @Test
    fun testTezCreation_Negative_BigDecimal() {
        val tez = Tez(BigDecimal("-2975671681509007947508815.2975671681509007947508815"))
        Assert.assertEquals(BigInteger("2975671681509007947508815"), tez.integerAmount)
        Assert.assertEquals(297567.toBigInteger(), tez.decimalAmount)
        Assert.assertEquals("2975671681509007947508815297567", tez.stringRepresentation)
        Assert.assertEquals(BigDecimal("2975671681509007947508815.297567"), tez.signedDecimalRepresentation)
    }
}
