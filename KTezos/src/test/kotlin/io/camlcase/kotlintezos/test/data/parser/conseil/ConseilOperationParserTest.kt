/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 */
package io.camlcase.kotlintezos.test.data.parser.conseil

import io.camlcase.kotlintezos.data.conseil.ConseilPredicateType
import io.camlcase.kotlintezos.data.parser.conseil.ConseilOperationListParser
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.conseil.ConseilOrigination
import io.camlcase.kotlintezos.model.conseil.ConseilReveal
import io.camlcase.kotlintezos.model.conseil.ConseilTransaction
import io.camlcase.kotlintezos.test.network.MockDispatcher
import org.junit.Assert
import org.junit.Test
import java.util.*

class ConseilOperationParserTest {
    @Test
    fun test_Get_Received_Transactions_For_Delegated_KT1() {
        val input = MockDispatcher.getStringFromFile("conseil_get_received_transactions_KT1.json")
        val result = ConseilOperationListParser().parse(input)

        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 2)
        Assert.assertTrue(result.firstOrNull { it == null } == null)
        Assert.assertTrue(result[0] is ConseilTransaction)
        Assert.assertTrue(result[0].fees == null)
        Assert.assertTrue(Date(result[0].timestamp).year + 1900 == 2020)
    }

    @Test
    fun test_Get_Sent_Transactions_For_Delegated_KT1() {
        val input = MockDispatcher.getStringFromFile("conseil_get_sent_transactions_KT1.json")
        val result = ConseilOperationListParser().parse(input)

        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 17)
        Assert.assertTrue(result.firstOrNull { it.status != ConseilPredicateType.OperationStatus.APPLIED } == null)
        Assert.assertTrue(result.firstOrNull { it !is ConseilTransaction } == null)
        Assert.assertTrue(result.firstOrNull { it == null } == null)
    }

    @Test
    fun test_Get_Received_Transactions_For_tz1() {
        val input = MockDispatcher.getStringFromFile("conseil_get_received_transactions.json")
        val result = ConseilOperationListParser().parse(input)

        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 3)
        Assert.assertTrue(result.firstOrNull { it == null } == null)
        Assert.assertTrue(result[0] is ConseilTransaction)
        Assert.assertTrue(result[0].fees!!.fee == Tez("14426"))
        Assert.assertTrue(Date(result[0].timestamp).year + 1900 == 2020)
    }

    @Test
    fun test_Get_Sent_Transactions_For_tz1() {
        val input = MockDispatcher.getStringFromFile("conseil_get_sent_transactions.json")
        val result = ConseilOperationListParser().parse(input)

        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 2)
        Assert.assertTrue(result.firstOrNull { it !is ConseilTransaction } == null)
        Assert.assertTrue(result.firstOrNull { it.status != ConseilPredicateType.OperationStatus.APPLIED } == null)
        Assert.assertTrue(result.firstOrNull { it == null } == null)
    }

    // Transactions + 1 delegation
    @Test
    fun test_Get_Received_Operations_For_tz1() {
        val input = MockDispatcher.getStringFromFile("conseil_get_received_operations.json")
        val result = ConseilOperationListParser().parse(input)

        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 6)
        Assert.assertTrue(result.firstOrNull { it == null } == null)
        Assert.assertTrue(result[0] is ConseilTransaction)
        Assert.assertTrue(result[0].fees!!.fee == Tez("60467"))
        Assert.assertTrue(result.firstOrNull { it !is ConseilTransaction } != null)
        Assert.assertTrue(Date(result[0].timestamp).year + 1900 == 2020)
    }

    @Test
    fun test_Get_Sent_Origination() {
        val input =
            "[{\"secret\":null,\"storage_size\":1350,\"number_of_slots\":null,\"utc_year\":2020,\"internal\":false,\"delegatable\":null,\"source\":\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\",\"consumed_gas\":43592,\"timestamp\":1584624679000,\"pkh\":null,\"nonce\":null,\"parameters_micheline\":null,\"utc_day\":19,\"block_level\":279190,\"branch\":null,\"utc_month\":3,\"errors\":null,\"balance\":0,\"operation_group_hash\":\"opUH9fQxJFtPgaK4wksZAuYoHTbrhYdXVcXqqTRDrRFPr2t1Udz\",\"public_key\":null,\"paid_storage_size_diff\":1350,\"amount\":null,\"delegate\":null,\"parameters_entrypoints\":null,\"utc_time\":\"13:31:19\",\"proposal\":null,\"block_hash\":\"BMBVUaUDcHumUL2Mjd5HMJYbiFq48z14e7NpHB5tcwoeEPR9MGY\",\"spendable\":null,\"cycle\":136,\"status\":\"applied\",\"operation_id\":3560914,\"manager_pubkey\":null,\"slots\":null,\"storage_limit\":1607,\"ballot_period\":null,\"storage\":\"Pair (Pair None None) \\\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\\\"\",\"counter\":553004,\"script\":\"parameter (or (or (lambda %do (option mutez) operation) (mutez %checkLimit)) (or (unit %default) (option %setLimit mutez)));\\nstorage (pair (pair (option %last_transaction (pair (timestamp %0) (mutez %expenses))) (option %limit mutez)) (key_hash %owner));\\ncode {}\",\"kind\":\"origination\",\"originated_contracts\":\"KT1JdDRnmhSfSHdcfheX1DLpNpiL2rhLm3nv\",\"gas_limit\":43692,\"parameters\":null,\"destination\":null,\"ballot\":null,\"period\":136,\"fee\":7432,\"level\":null}]"
        val result = ConseilOperationListParser().parse(input)

        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 1)
        Assert.assertTrue(result.firstOrNull { it == null } == null)
        Assert.assertTrue(result[0] is ConseilOrigination)
        Assert.assertTrue((result[0] as ConseilOrigination).originatedContract == "KT1JdDRnmhSfSHdcfheX1DLpNpiL2rhLm3nv")
        Assert.assertTrue((result[0] as ConseilOrigination).contractScript.startsWith("parameter (or (or (lamb"))
    }

    @Test
    fun test_Get_Sent_Reveal() {
        val input =
            "[{\"secret\":null,\"storage_size\":null,\"number_of_slots\":null,\"utc_year\":2020,\"internal\":false,\"delegatable\":null,\"source\":\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\",\"consumed_gas\":10000,\"timestamp\":1584617923000,\"pkh\":null,\"nonce\":null,\"parameters_micheline\":null,\"utc_day\":19,\"block_level\":278972,\"branch\":null,\"utc_month\":3,\"errors\":null,\"balance\":null,\"operation_group_hash\":\"opNqc59wZ3btos9TYDoMLNr8nes3vAPKCjaDSt2ft2kMrTtjHkt\",\"public_key\":\"edpkuyY5qaMNi9fKu3tvfZ7KcPnkvdyvZX3zq8vRLTLfFrpQhW3Qge\",\"paid_storage_size_diff\":null,\"amount\":null,\"delegate\":null,\"parameters_entrypoints\":null,\"utc_time\":\"11:38:43\",\"proposal\":null,\"block_hash\":\"BMCMMZ3fybEwzV2KSVdZmDsSeiStgfyKd5Y5s62z1YfLz5ZcDxF\",\"spendable\":null,\"cycle\":136,\"status\":\"applied\",\"operation_id\":3557867,\"manager_pubkey\":null,\"slots\":null,\"storage_limit\":0,\"ballot_period\":null,\"storage\":null,\"counter\":553002,\"script\":null,\"kind\":\"reveal\",\"originated_contracts\":null,\"gas_limit\":10000,\"parameters\":null,\"destination\":null,\"ballot\":null,\"period\":136,\"fee\":1268,\"level\":null}]"
        val result = ConseilOperationListParser().parse(input)

        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 1)
        Assert.assertTrue(result.firstOrNull { it == null } == null)
        Assert.assertTrue(result[0] is ConseilReveal)
        Assert.assertTrue((result[0] as ConseilReveal).publicKey == "edpkuyY5qaMNi9fKu3tvfZ7KcPnkvdyvZX3zq8vRLTLfFrpQhW3Qge")
    }
}
