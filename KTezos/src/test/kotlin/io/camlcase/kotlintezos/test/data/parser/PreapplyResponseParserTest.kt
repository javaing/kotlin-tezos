/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.kotlintezos.test.data.parser

import io.camlcase.kotlintezos.data.parser.PreapplyResponseParser
import io.camlcase.kotlintezos.test.network.MockDispatcher
import kotlinx.serialization.SerializationException
import org.junit.Assert
import org.junit.Test

internal class PreapplyResponseParserTest {

    @Test
    fun testPreapplicationValidOperation() {
        val result = PreapplyResponseParser().parse(VALID_PREAPPLY_JSON_RESPONSE)
        Assert.assertFalse(result == null)
        Assert.assertTrue(result?.error.isNullOrEmpty())
    }

    @Test
    fun testPreapplicationInvalidOperation() {
        val result = PreapplyResponseParser().parse(INVALID_PREAPPLY_JSON_RESPONSE)
        Assert.assertFalse(result?.error.isNullOrEmpty())
        Assert.assertTrue(result?.error?.size == 1)
        Assert.assertTrue(result?.error?.get(0)?.cause == "proto.003-PsddFKi3.contract.balance_too_low")
    }

    @Test(expected = SerializationException::class)
    fun testPreapplicationInvalidJson() {
        PreapplyResponseParser().parse("not json")
    }

    @Test
    fun testOrigination_Valid_Preapply() {
        val originationResult = MockDispatcher.getStringFromFile("preapply_origination_result.json")
        val result = PreapplyResponseParser().parse(originationResult)
        Assert.assertFalse(result?.error == null)
        Assert.assertTrue(result?.error!!.isEmpty())
        Assert.assertFalse(result.originatedContracts.isNullOrEmpty())
        Assert.assertTrue(result.originatedContracts!!.size == 1)
        Assert.assertEquals("KT1LHj4gvphdrhmvtWbAxqCJ2u7GvsKt87b9", result.originatedContracts!![0])
    }

    @Test
    fun testPreapply_Swap_TokenToXTZ() {
        val preapplyResult = MockDispatcher.getStringFromFile("preapply_swap_tokentoxtz_kt1_result.json")
        val result = PreapplyResponseParser().parse(preapplyResult)
        Assert.assertFalse(result?.error.isNullOrEmpty())
        Assert.assertTrue(result?.error?.size == 2)
        Assert.assertTrue(result?.error?.get(0)?.cause == "proto.005-PsBabyM1.michelson_v1.runtime_error")
        Assert.assertTrue(result?.error?.get(0)?.message.isNullOrEmpty())
        Assert.assertTrue(result?.error?.get(1)?.cause == "proto.005-PsBabyM1.michelson_v1.script_rejected")
        Assert.assertTrue(!result?.error?.get(1)?.message.isNullOrEmpty())
    }


    companion object {
        const val VALID_PREAPPLY_JSON_RESPONSE =
            "[{\"contents\":[{\"kind\":\"transaction\",\"source\":\"tz1XVJ8bZUXs7r5NV8dHvuiBhzECvLRLR3jW\",\"fee\":\"1272\",\"counter\":\"30802\",\"gas_limit\":\"10100\",\"storage_limit\":\"257\",\"amount\":\"10000000000000\",\"destination\":\"tz3WXYtyDUNL91qfiCJtVUX746QpNv5i5ve5\",\"metadata\":{\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1XVJ8bZUXs7r5NV8dHvuiBhzECvLRLR3jW\",\"change\":\"-1272\"},{\"kind\":\"freezer\",\"category\":\"fees\",\"delegate\":\"tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU\",\"level\":125,\"change\":\"1272\"}],\"operation_result\":{\"status\":\"success\",\"errors\":[]}}}],\"signature\":\"edsigu16pv1NUsXuJkwWDAqvFDbhcsRAHbdxbYJcN7AShN4yDspRmsP5kgbzs2osTHGGDkyED3vjQFcbskv3BVESJ7tpchmbbop\"}]"

        const val INVALID_PREAPPLY_JSON_RESPONSE =
            "[{\"contents\":[{\"kind\":\"transaction\",\"source\":\"tz1XVJ8bZUXs7r5NV8dHvuiBhzECvLRLR3jW\",\"fee\":\"1272\",\"counter\":\"30802\",\"gas_limit\":\"10100\",\"storage_limit\":\"257\",\"amount\":\"10000000000000\",\"destination\":\"tz3WXYtyDUNL91qfiCJtVUX746QpNv5i5ve5\",\"metadata\":{\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1XVJ8bZUXs7r5NV8dHvuiBhzECvLRLR3jW\",\"change\":\"-1272\"},{\"kind\":\"freezer\",\"category\":\"fees\",\"delegate\":\"tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU\",\"level\":125,\"change\":\"1272\"}],\"operation_result\":{\"status\":\"failed\",\"errors\":[{\"kind\":\"temporary\",\"id\":\"proto.003-PsddFKi3.contract.balance_too_low\",\"contract\":\"tz1XVJ8bZUXs7r5NV8dHvuiBhzECvLRLR3jW\",\"balance\":\"98751713\",\"amount\":\"10000000000000\"}]}}}],\"signature\":\"edsigu16pv1NUsXuJkwWDAqvFDbhcsRAHbdxbYJcN7AShN4yDspRmsP5kgbzs2osTHGGDkyED3vjQFcbskv3BVESJ7tpchmbbop\"}]"

    }
}
