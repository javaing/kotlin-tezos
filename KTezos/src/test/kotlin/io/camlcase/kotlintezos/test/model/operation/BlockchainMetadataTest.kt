/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.kotlintezos.test.model.operation

import io.camlcase.kotlintezos.model.TezosProtocol
import io.camlcase.kotlintezos.model.blockchain.BlockchainMetadata
import io.camlcase.kotlintezos.test.util.Params.Companion.testNetworkConstants
import org.junit.Assert
import org.junit.Test

class BlockchainMetadataTest {
    @Test
    fun testValidBabylonProtocol() {
        val metadata = BlockchainMetadata(
            "BMJ15hCiugQ17mo7g3bufaQsJqtxiWebqNYswLV3tdZB17X91WQ",
            "PsBabyM1eUXZseaJdmXFApDSBqj8YBfwELoxZHHW77EMcAbbwAS",
            "NetXdQprcVkpaWU",
            1234,
            null,
            testNetworkConstants
        )
        Assert.assertEquals(TezosProtocol.BABYLON, metadata.tezosProtocol)
    }

    @Test
    fun testValidCarthageProtocol() {
        val metadata = BlockchainMetadata(
            "BMECV4c854a1TEHApLLwfhJY6iHe5g71UutXTaaos4TPWDWr1K5",
            "PsCARTHAGazKbHtnKfLzQg3kms52kSRpgnDY982a9oYsSXRLQEb",
            "NetXjD3HPJJjmcd",
            1235,
            null,
            testNetworkConstants
        )
        Assert.assertEquals(TezosProtocol.CARTHAGE, metadata.tezosProtocol)
    }

    @Test
    fun testValidDelphinetProtocol() {
        val metadata = BlockchainMetadata(
            "BMECV4c854a1TEHApLLwfhJY6iHe5g71UutXTaaos4TPWDWr1K5",
            "PsDELPH1Kxsxt8f9eWbxQeRxkjfbxoqM52jvs5Y5fBxWWh4ifpo",
            "NetXjD3HPJJjmcd",
            1235,
            null,
            testNetworkConstants
        )
        Assert.assertEquals(TezosProtocol.DELPHI, metadata.tezosProtocol)
    }

    @Test
    fun testInvalidProtocol() {
        val metadata = BlockchainMetadata(
            "BMECV4c854a1TEHApLLwfhJY6iHe5g71UutXTaaos4TPWDWr1K5",
            "PsYaddaYadda",
            "NetXjD3HPJJjmcd",
            1235,
            null,
            testNetworkConstants
        )
        Assert.assertEquals(TezosProtocol.GRANADA, metadata.tezosProtocol)
    }
}
