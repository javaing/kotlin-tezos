/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.operation.fees

import io.camlcase.kotlintezos.core.ext.equalsToWildcard
import io.camlcase.kotlintezos.data.RunOperationRPC
import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.TezosErrorType
import io.camlcase.kotlintezos.model.operation.TransactionOperation
import io.camlcase.kotlintezos.model.operation.fees.AllocationFee
import io.camlcase.kotlintezos.model.operation.fees.BurnFee
import io.camlcase.kotlintezos.model.operation.fees.ExtraFeeType
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import io.camlcase.kotlintezos.model.toTez
import io.camlcase.kotlintezos.operation.ForgingPolicy
import io.camlcase.kotlintezos.operation.ForgingService
import io.camlcase.kotlintezos.operation.SimulationService
import io.camlcase.kotlintezos.operation.fees.FeeConstants
import io.camlcase.kotlintezos.operation.fees.FeeEstimatorService
import io.camlcase.kotlintezos.operation.fees.SafetyMargin
import io.camlcase.kotlintezos.operation.forge.ForgingVerifier
import io.camlcase.kotlintezos.operation.forge.RemoteForgingService
import io.camlcase.kotlintezos.test.network.MockServerTest
import io.camlcase.kotlintezos.test.network.error400
import io.camlcase.kotlintezos.test.network.error404
import io.camlcase.kotlintezos.test.network.validResponse
import io.camlcase.kotlintezos.test.util.CurrentThreadExecutor
import io.camlcase.kotlintezos.test.util.Params
import io.camlcase.kotlintezos.test.util.RPCConstants
import io.camlcase.kotlintezos.test.util.fakeSignature
import io.camlcase.kotlintezos.test.util.testFailure
import io.github.vjames19.futures.jdk8.Future
import io.mockk.confirmVerified
import io.mockk.every
import io.mockk.mockk
import io.mockk.spyk
import io.mockk.verify
import okhttp3.mockwebserver.Dispatcher
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.RecordedRequest
import org.junit.After
import org.junit.Assert
import org.junit.Test
import java.util.concurrent.ExecutorService

class FeeEstimatorServiceTest : MockServerTest() {

    private val executorService: ExecutorService = CurrentThreadExecutor()
    private val simulationService: SimulationService = spyk(SimulationService(mockClient, executorService))
    private val verifier = mockk<ForgingVerifier>(relaxed = true) {
        every { verify(any(), any(), any()) } returns Future { true }
    }
    private val remoteForging = spyk(
        RemoteForgingService(
            mockClient,
            verifier,
            executorService
        )
    )
    private val forgingService: ForgingService = spyk(ForgingService(remoteForging, executorService))

    @After
    fun after() {
        confirmVerified(simulationService)
        confirmVerified(forgingService)
    }

    private fun initService(): FeeEstimatorService {
        return spyk(FeeEstimatorService(executorService, simulationService, forgingService))
    }

    @Test
    fun `estimate Forge KO - NO extra fees`() {
        val json =
            "{\"contents\":[{\"kind\":\"transaction\",\"source\":\"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\",\"fee\":\"1477\",\"counter\":\"627192\",\"gas_limit\":\"10500\",\"storage_limit\":\"257\",\"amount\":\"500000\",\"destination\":\"tz1RKLWbGm7T4mnxDZHWazkbnvaryKsxxZTF\",\"metadata\":{\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\",\"change\":\"-1477\"},{\"kind\":\"freezer\",\"category\":\"fees\",\"delegate\":\"tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU\",\"cycle\":327,\"change\":\"1477\"}],\"operation_result\":{\"status\":\"applied\",\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\",\"change\":\"-500000\"},{\"kind\":\"contract\",\"contract\":\"tz1RKLWbGm7T4mnxDZHWazkbnvaryKsxxZTF\",\"change\":\"500000\"}],\"consumed_gas\":\"10207\"}}}]}"
        webServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when {
                    request.path?.equals(RunOperationRPC.RPC_PATH_RUN_OPERATION) == true ->
                        validResponse().setBody(json)
                    request.path?.equalsToWildcard(RPCConstants.RPC_FORGE_OPERATION) == true -> error400()
                    else -> error404()
                }
            }
        }

        val operation = TransactionOperation(
            Params.fakeTransactionParams.amount,
            Params.fakeTransactionParams.from,
            Params.fakeTransactionParams.to,
            OperationFees.simulationFees
        )

        val service = initService()
        service.estimate(
            listOf(operation),
            Params.fakeAddress,
            Params.fakeMetadata,
            fakeSignature
        ).testFailure(countDownLatch) {
            Assert.assertTrue((it as TezosError).type == TezosErrorType.RPC_ERROR)
        }

        countDownLatch.await()

        verify(exactly = 1) {
            simulationService.simulate(listOf(operation), Params.fakeAddress, Params.fakeMetadata, fakeSignature)
            forgingService.forge(ForgingPolicy.REMOTE, any(), Params.fakeMetadata)
        }
    }

    @Test
    fun `estimate OK - Storage Limit`() {
        val simulationJson =
            "{\"contents\":[{\"kind\":\"transaction\",\"source\":\"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\",\"fee\":\"0\",\"counter\":\"627191\",\"gas_limit\":\"800000\",\"storage_limit\":\"60000\",\"amount\":\"1000000\",\"destination\":\"KT1Ft3X9Lbdcc5GMCgvZcjFzGkeJY7sLKYqW\",\"parameters\":{\"entrypoint\":\"xtzToToken\",\"value\":{\"prim\":\"Pair\",\"args\":[{\"string\":\"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\"},{\"prim\":\"Pair\",\"args\":[{\"int\":\"6951\"},{\"string\":\"2020-08-25T12:28:12Z\"}]}]}},\"metadata\":{\"balance_updates\":[],\"operation_result\":{\"status\":\"applied\",\"storage\":{\"prim\":\"Pair\",\"args\":[{\"int\":\"10602\"},{\"prim\":\"Pair\",\"args\":[{\"prim\":\"Pair\",\"args\":[{\"prim\":\"False\"},{\"prim\":\"Pair\",\"args\":[{\"prim\":\"False\"},{\"int\":\"1000881018\"}]}]},{\"prim\":\"Pair\",\"args\":[{\"prim\":\"Pair\",\"args\":[{\"bytes\":\"0000471c8882bcf12586e640b7efa46c6ea1e0f4da9e\"},{\"bytes\":\"01811c1eab51c1fd74195346ab33ae00d63eac5c2700\"}]},{\"prim\":\"Pair\",\"args\":[{\"int\":\"8372892\"},{\"int\":\"1197050000\"}]}]}]}]},\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\",\"change\":\"-1000000\"},{\"kind\":\"contract\",\"contract\":\"KT1Ft3X9Lbdcc5GMCgvZcjFzGkeJY7sLKYqW\",\"change\":\"1000000\"}],\"consumed_gas\":\"276901\",\"storage_size\":\"9834\"},\"internal_operation_results\":[{\"kind\":\"transaction\",\"source\":\"KT1Ft3X9Lbdcc5GMCgvZcjFzGkeJY7sLKYqW\",\"nonce\":0,\"amount\":\"0\",\"destination\":\"KT1LMSRJukBcS5Z6XYaksA6FaFWX7f3B9n1d\",\"parameters\":{\"entrypoint\":\"transfer\",\"value\":{\"prim\":\"Pair\",\"args\":[{\"bytes\":\"01500d452eb9f13f627db4b7444974577e7d0d8dda00\"},{\"prim\":\"Pair\",\"args\":[{\"bytes\":\"0000ca1c5dd7f6665501b57c692f0726a1db46fd1d18\"},{\"int\":\"6979\"}]}]}},\"result\":{\"status\":\"applied\",\"storage\":{\"prim\":\"Pair\",\"args\":[{\"int\":\"10601\"},{\"int\":\"10000000\"}]},\"big_map_diff\":[{\"action\":\"update\",\"big_map\":\"10601\",\"key_hash\":\"exprtkFpPpLkPNbxReKT1osWCfseKDUyyxb7URVBaX2Yw1N5Sq86hv\",\"key\":{\"bytes\":\"0000ca1c5dd7f6665501b57c692f0726a1db46fd1d18\"},\"value\":{\"prim\":\"Pair\",\"args\":[[],{\"int\":\"6979\"}]}},{\"action\":\"update\",\"big_map\":\"10601\",\"key_hash\":\"expruEgEj53rAwspqjPjBN9f8eU5MDy8WuGUxUZs81UQroNHFVXPUi\",\"key\":{\"bytes\":\"01500d452eb9f13f627db4b7444974577e7d0d8dda00\"},\"value\":{\"prim\":\"Pair\",\"args\":[[],{\"int\":\"8372892\"}]}}],\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1e4hAp7xpjekmXnYe4677ELGA3UxR79EFb\",\"change\":\"-75000\"}],\"consumed_gas\":\"94445\",\"storage_size\":\"3638\",\"paid_storage_size_diff\":\"75\"}}]}}]}"
        webServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when {
                    request.path?.equals(RunOperationRPC.RPC_PATH_RUN_OPERATION) == true ->
                        validResponse().setBody(simulationJson)
                    request.path?.equalsToWildcard(RPCConstants.RPC_FORGE_OPERATION) == true -> validResponse().setBody(
                        "16f7879e79ca9aff12eeaf33083d0f3333709b5c2733ea4cde139c8e2ab115f96c00ca1c5dd7f6665501b57c692f0726a1db46fd1d1800f6a3260000c0843d01500d452eb9f13f627db4b7444974577e7d0d8dda00ffff0a78747a546f546f6b656e0000004907070100000024747a3165346841703778706a656b6d586e596534363737454c4741335578523739454662070700a76c0100000014323032302d30382d32355431323a32383a31325a"
                    )
                    else -> error404()
                }
            }
        }

        val operation = TransactionOperation(
            Params.fakeTransactionParams.amount,
            Params.fakeTransactionParams.from,
            Params.fakeTransactionParams.to,
            OperationFees.simulationFees
        )

        val service = initService()
        val result = service.estimate(
            listOf(operation),
            Params.fakeAddress,
            Params.fakeMetadata,
            fakeSignature
        ).join()

        println(result)
        Assert.assertNotNull(result)
        val accumulatedFees = result.accumulatedFees
        Assert.assertEquals(Tez("37574"), accumulatedFees.fee)
        Assert.assertEquals(371_346 + SafetyMargin.GAS, accumulatedFees.gasLimit)
        Assert.assertEquals(332, accumulatedFees.storageLimit)
        Assert.assertTrue(accumulatedFees.extraFees!!.asList.size == 1)
        Assert.assertTrue(accumulatedFees.extraFees!!.asList[0] is BurnFee)
        Assert.assertTrue(result.internalErrors.isNullOrEmpty())

        verify(exactly = 1) {
            simulationService.simulate(listOf(operation), Params.fakeAddress, Params.fakeMetadata, fakeSignature)
            forgingService.forge(ForgingPolicy.REMOTE, any(), Params.fakeMetadata)
        }
    }

    @Test
    fun `estimate OK - Extra allocation fee`() {
        val simulationJson =
            "{\"contents\":[{\"kind\":\"transaction\",\"source\":\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\",\"fee\":\"0\",\"counter\":\"553198\",\"gas_limit\":\"800000\",\"storage_limit\":\"60000\",\"amount\":\"10000000\",\"destination\":\"tz1ZzvMhZSsYP16QQEuTwhG1M2vYry344e5A\",\"metadata\":{\"balance_updates\":[],\"operation_result\":{\"status\":\"applied\",\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\",\"change\":\"-10000000\"},{\"kind\":\"contract\",\"contract\":\"tz1ZzvMhZSsYP16QQEuTwhG1M2vYry344e5A\",\"change\":\"10000000\"},{\"kind\":\"contract\",\"contract\":\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\",\"change\":\"-257000\"}],\"consumed_gas\":\"10207\",\"allocated_destination_contract\":true}}}]}"
        webServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when {
                    request.path?.equals(RunOperationRPC.RPC_PATH_RUN_OPERATION) == true ->
                        validResponse().setBody(simulationJson)
                    request.path?.equalsToWildcard(RPCConstants.RPC_FORGE_OPERATION) == true -> validResponse().setBody(
                        "8fc6ea354aca5caf342e0893ddc960c8384a31a8a81eb00493afc551a488556d6c0081744d3ad071c4fd31913c5e80da1c6c9fb9f4b300eee12180ea30e0d40380ade20400009d853548668b4fe3081e5c45492a2b72f646628800"
                    )
                    else -> error404()
                }
            }
        }

        val operation = TransactionOperation(
            Params.fakeTransactionParams.amount,
            Params.fakeTransactionParams.from,
            Params.fakeTransactionParams.to,
            OperationFees.simulationFees
        )

        val service = initService()
        val result = service.estimate(
            listOf(operation),
            Params.fakeAddress,
            Params.fakeMetadata,
            fakeSignature
        ).join()

        Assert.assertNotNull(result)
        val accumulatedFees = result.accumulatedFees
        Assert.assertEquals(Tez("1376"), accumulatedFees.fee)
        Assert.assertEquals(10_207 + SafetyMargin.GAS, accumulatedFees.gasLimit)
        Assert.assertEquals(257, accumulatedFees.storageLimit)
        val extraFee = accumulatedFees.extraFees!!.asList[0]
        Assert.assertEquals(Tez("257000"), extraFee.fee)
        Assert.assertTrue(extraFee is AllocationFee)
        Assert.assertTrue(result.internalErrors.isNullOrEmpty())

        verify(exactly = 1) {
            simulationService.simulate(listOf(operation), Params.fakeAddress, Params.fakeMetadata, fakeSignature)
            forgingService.forge(ForgingPolicy.REMOTE, any(), Params.fakeMetadata)
        }
    }

    @Test
    fun `estimate OK - Smart Contract + Reveal + Storage limit`() {
        val simulationJson =
            "{\"contents\":[{\"kind\":\"reveal\",\"source\":\"tz1ZzvMhZSsYP16QQEuTwhG1M2vYry344e5A\",\"fee\":\"1268\",\"counter\":\"3054764\",\"gas_limit\":\"10000\",\"storage_limit\":\"0\",\"public_key\":\"edpktm4QLWFp492q19cVztneJ6w9MacvsEurYyZHeCU9MdVnB9egFr\",\"metadata\":{\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1ZzvMhZSsYP16QQEuTwhG1M2vYry344e5A\",\"change\":\"-1268\"},{\"kind\":\"freezer\",\"category\":\"fees\",\"delegate\":\"tz1Ke2h7sDdakHJQh8WX4Z372du1KChsksyU\",\"cycle\":384,\"change\":\"1268\"}],\"operation_result\":{\"status\":\"applied\",\"consumed_gas\":\"10000\"}}},{\"kind\":\"transaction\",\"source\":\"tz1ZzvMhZSsYP16QQEuTwhG1M2vYry344e5A\",\"fee\":\"0\",\"counter\":\"3054765\",\"gas_limit\":\"800000\",\"storage_limit\":\"60000\",\"amount\":\"5000000\",\"destination\":\"KT1QwuYp1g9uvii5CPXivNQd4RC7VGvLPS4e\",\"parameters\":{\"entrypoint\":\"xtzToToken\",\"value\":{\"prim\":\"Pair\",\"args\":[{\"string\":\"tz1ZzvMhZSsYP16QQEuTwhG1M2vYry344e5A\"},{\"prim\":\"Pair\",\"args\":[{\"int\":\"52\"},{\"string\":\"2020-10-08T10:53:02Z\"}]}]}},\"metadata\":{\"balance_updates\":[],\"operation_result\":{\"status\":\"applied\",\"storage\":{\"prim\":\"Pair\",\"args\":[{\"int\":\"18981\"},{\"prim\":\"Pair\",\"args\":[{\"prim\":\"Pair\",\"args\":[{\"prim\":\"False\"},{\"prim\":\"Pair\",\"args\":[{\"prim\":\"False\"},{\"int\":\"43436821\"}]}]},{\"prim\":\"Pair\",\"args\":[{\"prim\":\"Pair\",\"args\":[{\"bytes\":\"0000471c8882bcf12586e640b7efa46c6ea1e0f4da9e\"},{\"bytes\":\"01948958994aa341678798cd5b64d2073324a23cee00\"}]},{\"prim\":\"Pair\",\"args\":[{\"int\":\"33097\"},{\"int\":\"3138111493\"}]}]}]}]},\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1ZzvMhZSsYP16QQEuTwhG1M2vYry344e5A\",\"change\":\"-5000000\"},{\"kind\":\"contract\",\"contract\":\"KT1QwuYp1g9uvii5CPXivNQd4RC7VGvLPS4e\",\"change\":\"5000000\"}],\"consumed_gas\":\"290011\",\"storage_size\":\"12288\"},\"internal_operation_results\":[{\"kind\":\"transaction\",\"source\":\"KT1QwuYp1g9uvii5CPXivNQd4RC7VGvLPS4e\",\"nonce\":0,\"amount\":\"0\",\"destination\":\"KT1N8A78V9fSiyGwqBpAU2ZQ6S446C7ZwRoD\",\"parameters\":{\"entrypoint\":\"transfer\",\"value\":{\"prim\":\"Pair\",\"args\":[{\"bytes\":\"01b381697d7811f126f77945d42aae7fc9e5398f3e00\"},{\"prim\":\"Pair\",\"args\":[{\"bytes\":\"00009d853548668b4fe3081e5c45492a2b72f6466288\"},{\"int\":\"52\"}]}]}},\"result\":{\"status\":\"applied\",\"storage\":{\"prim\":\"Pair\",\"args\":[{\"int\":\"18980\"},{\"int\":\"50000000\"}]},\"big_map_diff\":[{\"action\":\"update\",\"big_map\":\"18980\",\"key_hash\":\"exprvSXNipospnQsdNbWfiH6oAiPVFbPm3KoRZgmgc1pUSpVMABMy7\",\"key\":{\"bytes\":\"00009d853548668b4fe3081e5c45492a2b72f6466288\"},\"value\":{\"prim\":\"Pair\",\"args\":[{\"int\":\"52\"},[]]}},{\"action\":\"update\",\"big_map\":\"18980\",\"key_hash\":\"expru2GH3fVCedsabmg7cYwN21Zdzat418z4EHVbh17hpPMKB47pNF\",\"key\":{\"bytes\":\"01b381697d7811f126f77945d42aae7fc9e5398f3e00\"},\"value\":{\"prim\":\"Pair\",\"args\":[{\"int\":\"33097\"},[]]}}],\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1ZzvMhZSsYP16QQEuTwhG1M2vYry344e5A\",\"change\":\"-74000\"}],\"consumed_gas\":\"56275\",\"storage_size\":\"4374\",\"paid_storage_size_diff\":\"74\"}}]}}]}"
        webServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when {
                    request.path?.equals(RunOperationRPC.RPC_PATH_RUN_OPERATION) == true ->
                        validResponse().setBody(simulationJson)
                    request.path?.equalsToWildcard(RPCConstants.RPC_FORGE_OPERATION) == true -> validResponse().setBody(
                        "f04830a3cec208ae5aae370712ccd8ae82d569c5d621f7350a254fe2ff2f55046b009d853548668b4fe3081e5c45492a2b72f6466288f409acb9ba01904e00000f85685fff96d526012993f295b8937796dd357b455e2e78ad60906a65b3cb566c009d853548668b4fe3081e5c45492a2b72f646628800adb9ba0180ea30e0d403c096b10201b381697d7811f126f77945d42aae7fc9e5398f3e00ffff0a78747a546f546f6b656e0000004807070100000024747a315a7a764d685a5373595031365151457554776847314d3276597279333434653541070700340100000014323032302d31302d30385431303a35333a30325a"
                    )
                    else -> error404()
                }
            }
        }

        val operation = TransactionOperation(
            Params.fakeTransactionParams.amount,
            Params.fakeTransactionParams.from,
            Params.fakeTransactionParams.to,
            OperationFees.simulationFees
        )

        val service = initService()
        val result = service.estimate(
            listOf(operation),
            Params.fakeAddress,
            Params.fakeMetadata,
            fakeSignature
        ).join()

        println(result)
        Assert.assertNotNull(result)
        val accumulatedFees = result.accumulatedFees
        Assert.assertEquals(Tez("36645"), accumulatedFees.fee)
        Assert.assertEquals(356_686 + SafetyMargin.GAS, accumulatedFees.gasLimit)
        Assert.assertEquals(588, accumulatedFees.storageLimit)
        Assert.assertTrue(accumulatedFees.extraFees!!.asList.size == 1)
        Assert.assertTrue(accumulatedFees.extraFees!!.asList[0] is BurnFee)
        Assert.assertTrue(result.internalErrors.isNullOrEmpty())

        verify(exactly = 1) {
            simulationService.simulate(listOf(operation), Params.fakeAddress, Params.fakeMetadata, fakeSignature)
            forgingService.forge(ForgingPolicy.REMOTE, any(), Params.fakeMetadata)
        }
    }

    @Test
    fun `calculateBakerFee Transaction`() {
        var result = FeeEstimatorService.calculateBakerFee(155, 10307)
        Assert.assertEquals(Tez("1386"), result)

        result = FeeEstimatorService.calculateBakerFee(308, 346_386)
        Assert.assertEquals(Tez("35147"), result)

        // Should be less than the one before since operationSize = 0
        result = FeeEstimatorService.calculateBakerFee(0, 346_386)
        Assert.assertEquals(Tez("34839"), result)

        result = FeeEstimatorService.calculateBakerFee(0, 0)
        Assert.assertEquals(FeeConstants.MINIMAL_FEE.toTez(), result)
    }

    @Test
    fun `estimate KO +Max XTZ Balance`() {
        val simulationJson =
            "{\"contents\":[{\"kind\":\"transaction\",\"source\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\",\"fee\":\"0\",\"counter\":\"14732\",\"gas_limit\":\"800000\",\"storage_limit\":\"60000\",\"amount\":\"19867517615\",\"destination\":\"tz1ZebN9N7fB5y7CQpSFw3xf1KpaX1auVF7Z\",\"metadata\":{\"balance_updates\":[],\"operation_result\":{\"status\":\"backtracked\",\"errors\":[{\"kind\":\"temporary\",\"id\":\"proto.008-PtEdo2Zk.contract.cannot_pay_storage_fee\"},{\"kind\":\"temporary\",\"id\":\"proto.008-PtEdo2Zk.contract.balance_too_low\",\"contract\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\",\"balance\":\"1285\",\"amount\":\"64250\"}],\"balance_updates\":[{\"kind\":\"contract\",\"contract\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\",\"change\":\"-19867517615\"},{\"kind\":\"contract\",\"contract\":\"tz1ZebN9N7fB5y7CQpSFw3xf1KpaX1auVF7Z\",\"change\":\"19867517615\"},{\"kind\":\"contract\",\"contract\":\"tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5\",\"change\":\"-64250\"}],\"consumed_gas\":\"1427\",\"consumed_milligas\":\"1427000\",\"allocated_destination_contract\":true}}}]}"

        webServer.dispatcher = object : Dispatcher() {
            override fun dispatch(request: RecordedRequest): MockResponse {
                return when {
                    request.path?.equals(RunOperationRPC.RPC_PATH_RUN_OPERATION) == true ->
                        validResponse().setBody(simulationJson)
                    request.path?.equalsToWildcard(RPCConstants.RPC_FORGE_OPERATION) == true -> validResponse().setBody(
                        "f04830a3cec208ae5aae370712ccd8ae82d569c5d621f7350a254fe2ff2f55046b009d853548668b4fe3081e5c45492a2b72f6466288f409acb9ba01904e00000f85685fff96d526012993f295b8937796dd357b455e2e78ad60906a65b3cb566c009d853548668b4fe3081e5c45492a2b72f646628800adb9ba0180ea30e0d403c096b10201b381697d7811f126f77945d42aae7fc9e5398f3e00ffff0a78747a546f546f6b656e0000004807070100000024747a315a7a764d685a5373595031365151457554776847314d3276597279333434653541070700340100000014323032302d31302d30385431303a35333a30325a"
                    )
                    else -> error404()
                }
            }
        }

        val operation = TransactionOperation(
            Params.fakeTransactionParams.amount,
            Params.fakeTransactionParams.from,
            Params.fakeTransactionParams.to,
            OperationFees.simulationFees
        )

        val service = initService()
        val result = service.estimate(
            listOf(operation),
            Params.fakeAddress,
            Params.fakeMetadata,
            fakeSignature
        ).join()

        println(result)
        Assert.assertNotNull(result)
        val accumulatedFees = result.accumulatedFees
        Assert.assertEquals(Tez("651"), accumulatedFees.fee)
        Assert.assertEquals(1827, accumulatedFees.gasLimit)
        Assert.assertEquals(257, accumulatedFees.storageLimit)
        Assert.assertTrue(accumulatedFees.extraFees!!.asList.size == 1)
        Assert.assertTrue(accumulatedFees.extraFees!!.getFee(ExtraFeeType.ALLOCATION_FEE) == Tez("64250"))
        Assert.assertTrue(result.internalErrors!!.size == 2)
        Assert.assertTrue(result.internalErrors!![0].cause == "proto.008-PtEdo2Zk.contract.cannot_pay_storage_fee")
        Assert.assertTrue(result.internalErrors!![1].cause == "proto.008-PtEdo2Zk.contract.balance_too_low")

        verify(exactly = 1) {
            simulationService.simulate(listOf(operation), Params.fakeAddress, Params.fakeMetadata, fakeSignature)
            forgingService.forge(ForgingPolicy.REMOTE, any(), Params.fakeMetadata)
        }
    }
}
