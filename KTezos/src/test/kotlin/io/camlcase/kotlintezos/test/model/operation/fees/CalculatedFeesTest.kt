/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.model.operation.fees

import io.camlcase.kotlintezos.model.Tez
import io.camlcase.kotlintezos.model.operation.fees.CalculatedFees
import io.camlcase.kotlintezos.model.operation.fees.OperationFees
import org.junit.Assert
import org.junit.Test

class CalculatedFeesTest {
    @Test
    fun `constructor default`() {
        val obj = CalculatedFees(fees, accumulated)
        Assert.assertTrue(obj.operationsFees == fees)
        Assert.assertTrue(obj.accumulatedFees == accumulated)
    }

    @Test
    fun `constructor one arg`() {
        val obj = CalculatedFees(fees)
        Assert.assertTrue(obj.operationsFees == fees)
        Assert.assertTrue(obj.accumulatedFees == accumulated)
    }

    @Test
    fun `constructor negative values`() {
        val fees = listOf(
            OperationFees(Tez(1.0), 200, 300),
            OperationFees(Tez(2.0), -500, 100),
            OperationFees(Tez(3.0), 400, -150)
        )
        val obj = CalculatedFees(Companion.fees)
        Assert.assertTrue(obj.operationsFees == Companion.fees)
        Assert.assertTrue(obj.accumulatedFees == accumulated)
    }

    companion object {
        val fees = listOf(
            OperationFees(Tez(1.0), 200, 300),
            OperationFees(Tez(2.0), 500, 100),
            OperationFees(Tez(3.0), 400, 150)
        )
        val accumulated = OperationFees(Tez(6.0), 1100, 550)
    }
}
