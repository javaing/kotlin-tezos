/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.ext

import io.camlcase.kotlintezos.core.ext.parseGeneralErrors
import io.camlcase.kotlintezos.data.Parser
import io.camlcase.kotlintezos.model.RPCErrorType
import org.junit.Assert
import org.junit.Test

class RPCExtTest {
    private val testParser = object : Parser<Any> {
        override fun parse(jsonData: String): Any? {
            // Nothing to do
            return null
        }
    }

    @Test
    fun testParseGeneralErrorsArray() {
        val input =
            "[{\"kind\":\"branch\",\"id\":\"proto.current_protocol.error1.description\",\"contract\":\"tz1(...))\"},{\"kind\":\"branch\",\"id\":\"proto.current_protocol.error2.description\",\"contract\":\"tz1(...))\"}]"
        val result = testParser.parseGeneralErrors(input)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 2)
        Assert.assertTrue(result[0].contract == "tz1(...))")
        Assert.assertTrue(result[0].type == RPCErrorType.BRANCH)
        Assert.assertTrue(result[0].cause == "proto.current_protocol.error1.description")
    }

    @Test
    fun testParseObjectError() {
        val input = "{\"message\":\"Missing Authentication Token\"}"
        val result = testParser.parseGeneralErrors(input)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 1)
        Assert.assertTrue(result[0].contract == null)
        Assert.assertTrue(result[0].type == RPCErrorType.TEMPORARY)
        Assert.assertTrue(result[0].cause == "Missing Authentication Token")
    }

    @Test
    fun test_Long_Error_message() {
        val input = "Failed to parse the request body: No case matched:\n" +
                "  At /kind, unexpected string instead of endorsement\n" +
                "  At /kind, unexpected string instead of seed_nonce_revelation\n" +
                "  At /kind, unexpected string instead of double_endorsement_evidence\n" +
                "  At /kind, unexpected string instead of double_baking_evidence\n" +
                "  At /kind, unexpected string instead of activate_account\n" +
                "  At /kind, unexpected string instead of proposals\n" +
                "  At /kind, unexpected string instead of ballot\n" +
                "  At /public_key:\n" +
                "    Unhandled error \"Assert_failure src/lib_crypto/base58.ml:217:6\"\n" +
                "  At /kind, unexpected string instead of transaction\n" +
                "  At /kind, unexpected string instead of origination\n" +
                "  At /kind, unexpected string instead of delegation"

        val result = testParser.parseGeneralErrors(input)
        Assert.assertNotNull(result)
        Assert.assertTrue(result!!.size == 1)
    }
}
