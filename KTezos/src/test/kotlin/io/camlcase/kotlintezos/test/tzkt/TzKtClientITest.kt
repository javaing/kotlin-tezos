/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.tzkt

import io.camlcase.kotlintezos.TzKtClient
import io.camlcase.kotlintezos.model.OperationResultStatus
import io.camlcase.kotlintezos.model.TezosCallback
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.tzkt.TzKtAccounType
import io.camlcase.kotlintezos.model.tzkt.TzKtAccount
import io.camlcase.kotlintezos.model.tzkt.TzKtOperation
import io.camlcase.kotlintezos.model.tzkt.TzKtReveal
import io.camlcase.kotlintezos.model.tzkt.TzKtTransaction
import io.camlcase.kotlintezos.network.DefaultNetworkClient
import io.camlcase.kotlintezos.smartcontract.michelson.PairMichelsonParameter
import io.mockk.spyk
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import java.io.IOException
import java.util.concurrent.CountDownLatch

/**
 * Test against a real api
 */
class TzKtClientITest {
    internal lateinit var countDownLatch: CountDownLatch

    @Before
    fun setup() {
        countDownLatch = CountDownLatch(1)
    }

    private fun initClient(url: String = TzKtClient.EDO2NET_TZKT_URL): TzKtClient {
        val client = DefaultNetworkClient(url, true)
        return spyk(TzKtClient(client))
    }

    @Test
    fun `getOperationDetails Reveal + Transaction`() {
        val client = initClient(TzKtClient.MAINNET_TZKT_URL)
        client.getOperationDetails(
            "ooZiSSKR5BMxHnJ9Prm2HyHGvfbQ29TbatZrWuFHSxhcehJDvzw",
            object : TezosCallback<List<TzKtOperation>> {
                override fun onSuccess(item: List<TzKtOperation>?) {
                    println(item)
                    Assert.assertNotNull(item)
                    Assert.assertTrue(item!!.size == 2)
                    Assert.assertTrue(item[0] is TzKtReveal)
                    Assert.assertTrue(item[0].status == OperationResultStatus.APPLIED)

                    val tzKtOp = item[0] as TzKtReveal
                    Assert.assertTrue(tzKtOp.source!!.address == "tz1PUgGvY15NWURR1iS5xLz12Byw6kTdqKu9")

                    Assert.assertTrue(item[1] is TzKtTransaction)
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.fail()
                    countDownLatch.countDown()
                }
            }
        )
        countDownLatch.await()
    }

    @Test
    fun `getOperationDetails Invalid hash`() {
        val client = initClient()
        client.getOperationDetails(
            "oo",
            object : TezosCallback<List<TzKtOperation>> {
                override fun onSuccess(item: List<TzKtOperation>?) {
//                    println(item)
                    Assert.fail()
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.assertTrue(error.exception is IOException)
                    Assert.assertTrue(error.code == 400)
                    countDownLatch.countDown()
                }

            }
        )
        countDownLatch.await()
    }

    @Test
    fun `getOperationDetails Failed Dexter Swap`() {
        val client = initClient(TzKtClient.EDO2NET_TZKT_URL)
        client.getOperationDetails(
            "opXsDAMbVnoaakccNW8rdow5NzDVyUXSZJAZFGzNT6hQUYqSt5R",
            object : TezosCallback<List<TzKtOperation>> {
                override fun onSuccess(item: List<TzKtOperation>?) {
//                    println(item)
                    Assert.assertNotNull(item)
                    Assert.assertTrue(item!!.size == 3)
                    val tzKtOp2 = item[1] as TzKtTransaction
                    Assert.assertTrue(tzKtOp2.status == OperationResultStatus.FAILED)
                    Assert.assertTrue(tzKtOp2.smartContractCall != null)
                    Assert.assertTrue(tzKtOp2.smartContractCall!!.entrypoint == "transfer")
                    Assert.assertTrue(tzKtOp2.smartContractCall!!.parameters is PairMichelsonParameter)
                    Assert.assertTrue(tzKtOp2.errors!!.isNotEmpty())
                    Assert.assertTrue(tzKtOp2.errors!![0].type == "gas_exhausted.operation")

                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.fail()
                    countDownLatch.countDown()
                }
            }
        )
        countDownLatch.await()
    }

    @Test
    fun `operations Baker with delegations`() {
        val client = initClient()
        client.operations(
            "tz1YCYVCQqFCtjQAQH5sTkkCkPkdMBQTfEn5",
            callback = object : TezosCallback<List<TzKtOperation>> {
                override fun onSuccess(item: List<TzKtOperation>?) {
                    Assert.assertNotNull(item)
                    // 100 limit for general operations + N value of Fa1.2 token received
                    Assert.assertTrue(item!!.size > 70) // TODO Make more than 100 operations
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.fail()
                    countDownLatch.countDown()
                }

            }
        )
        countDownLatch.await()
    }

    @Test
    fun `operations Empty account`() {
        val client = initClient()
        client.operations(
            "tz1ZebN9N7fB5y7CQpSFw3xf1KpaX1auVF7Z",
            callback = object : TezosCallback<List<TzKtOperation>> {
                override fun onSuccess(item: List<TzKtOperation>?) {
                    Assert.assertNotNull(item)
                    Assert.assertTrue(item!!.isEmpty())
                    countDownLatch.countDown()
                }

                override fun onFailure(error: TezosError) {
                    Assert.fail()
                    countDownLatch.countDown()
                }

            }
        )
        countDownLatch.await()
    }

    @Test
    fun `getAccounts Empty`() {
        val client = initClient()
        client.getAccount(
                "tz1ZebN9N7fB5y7CQpSFw3xf1KpaX1auVF78",
                callback = object : TezosCallback<TzKtAccount> {
                    override fun onSuccess(item: TzKtAccount?) {
                        Assert.assertNotNull(item)
                        Assert.assertTrue(item!!.type == TzKtAccounType.EMPTY)
                        countDownLatch.countDown()
                    }

                    override fun onFailure(error: TezosError) {
                        Assert.fail()
                        countDownLatch.countDown()
                    }
                }
        )
        countDownLatch.await()
    }
}
