/**
 * # Released under MIT License
 *
 * Copyright (c) 2020 camlCase
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated
 * documentation files (the "Software"), to deal in the Software without restriction, including without limitation the
 * rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all copies or substantial portions of
 * the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO
 * THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package io.camlcase.kotlintezos.test.data.parser

import io.camlcase.kotlintezos.data.parser.ParsedOperationParser
import io.camlcase.kotlintezos.model.TezosError
import io.camlcase.kotlintezos.model.operation.OperationType
import io.camlcase.kotlintezos.model.operation.SmartContractCallOperation
import io.camlcase.kotlintezos.smartcontract.michelson.PairMichelsonParameter
import io.camlcase.kotlintezos.test.network.MockDispatcher
import org.junit.Assert
import org.junit.Test

class ParsedOperationParserTest {
    @Test
    fun parse_Origination_Response() {
        val input = MockDispatcher.getStringFromFile("parse_operation_origination_result.json")
        val result = ParsedOperationParser().parse(input)

        Assert.assertNotNull(result)
        Assert.assertEquals(1, result!!.size)
        Assert.assertEquals(OperationType.ORIGINATION, result[0].type)
    }

    @Test
    fun parse_Transaction_SEND_Response() {
        val input = MockDispatcher.getStringFromFile("parse_operation_send_result.json")
        val result = ParsedOperationParser().parse(input)

        Assert.assertNotNull(result)
        Assert.assertEquals(1, result!!.size)
        Assert.assertEquals(OperationType.TRANSACTION, result[0].type)
    }

    @Test
    fun parse_Reveal_Response() {
        val input = MockDispatcher.getStringFromFile("parse_operation_reveal_result.json")
        val result = ParsedOperationParser().parse(input)

        Assert.assertNotNull(result)
        Assert.assertEquals(1, result!!.size)
        Assert.assertEquals(OperationType.REVEAL, result[0].type)
    }

    @Test
    fun parse_Delegate_Unrevealed_Response() {
        val input =
            "[{\"branch\":\"BMFxxeUTU1THNiCPh7rSpFvCGLMCna1Gz1zrJXxcoKCBy9XyTLR\",\"contents\":[{\"kind\":\"reveal\",\"source\":\"tz1Zccb24KHCe99goSnFpg4xh9JZNTHSNPTz\",\"fee\":\"1268\",\"counter\":\"553025\",\"gas_limit\":\"10000\",\"storage_limit\":\"0\",\"public_key\":\"edpktmE2AYPGiyM5pWpBzu5DTPx6rEBC1Foq3txYdxgNZGxdLs9oW8\"},{\"kind\":\"delegation\",\"source\":\"tz1Zccb24KHCe99goSnFpg4xh9JZNTHSNPTz\",\"fee\":\"1257\",\"counter\":\"553026\",\"gas_limit\":\"10000\",\"storage_limit\":\"0\",\"delegate\":\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\"}],\"signature\":\"edsigtXomBKi5CTRf5cjATJWSyaRvhfYNHqSUGrn4SdbYRcGwQrUGjzEfQDTuqHhuA8b2d8NarZjz8TRf65WkpQmo423BtomS8Q\"}]"
        val result = ParsedOperationParser().parse(input)

        Assert.assertNotNull(result)
        Assert.assertEquals(2, result!!.size)
        Assert.assertEquals(OperationType.REVEAL, result[0].type)
        Assert.assertEquals(OperationType.DELEGATION, result[1].type)
    }

    @Test
    fun parse_Invalid_Response() {
        // Reveal +
        // Delegation without delegate
        val input =
            "[{\"branch\":\"BMFxxeUTU1THNiCPh7rSpFvCGLMCna1Gz1zrJXxcoKCBy9XyTLR\",\"contents\":[{\"kind\":\"reveal\",\"source\":\"tz1Zccb24KHCe99goSnFpg4xh9JZNTHSNPTz\",\"fee\":\"1268\",\"counter\":\"553025\",\"gas_limit\":\"10000\",\"storage_limit\":\"0\",\"public_key\":\"edpktmE2AYPGiyM5pWpBzu5DTPx6rEBC1Foq3txYdxgNZGxdLs9oW8\"},{\"kind\":\"delegation\",\"source\":\"tz1Zccb24KHCe99goSnFpg4xh9JZNTHSNPTz\",\"fee\":\"1257\",\"counter\":\"553026\",\"gas_limit\":\"10000\",\"storage_limit\":\"0\"}],\"signature\":\"edsigtXomBKi5CTRf5cjATJWSyaRvhfYNHqSUGrn4SdbYRcGwQrUGjzEfQDTuqHhuA8b2d8NarZjz8TRf65WkpQmo423BtomS8Q\"}]"
        val result = ParsedOperationParser().parse(input)

        Assert.assertNotNull(result)
        Assert.assertEquals(1, result!!.size)
        Assert.assertEquals(OperationType.REVEAL, result[0].type)
    }

    @Test
    fun parse_Invalid_Response_2() {
        // metadata instead of "contents"
        val input =
            "[{\"branch\":\"BMFxxeUTU1THNiCPh7rSpFvCGLMCna1Gz1zrJXxcoKCBy9XyTLR\",\"metadata\":[{\"kind\":\"reveal\",\"source\":\"tz1Zccb24KHCe99goSnFpg4xh9JZNTHSNPTz\",\"fee\":\"1268\",\"counter\":\"553025\",\"gas_limit\":\"10000\",\"storage_limit\":\"0\",\"public_key\":\"edpktmE2AYPGiyM5pWpBzu5DTPx6rEBC1Foq3txYdxgNZGxdLs9oW8\"}],\"signature\":\"edsigtXomBKi5CTRf5cjATJWSyaRvhfYNHqSUGrn4SdbYRcGwQrUGjzEfQDTuqHhuA8b2d8NarZjz8TRf65WkpQmo423BtomS8Q\"}]"
        val result = ParsedOperationParser().parse(input)

        Assert.assertNull(result)
    }

    @Test(expected = TezosError::class)
    fun parse_Error_Response() {
        val input =
            "[{\"kind\":\"branch\",\"id\":\"proto.005-PsBabyM1.contract.unrevealed_key\",\"contract\":\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\"}]"
        val result = ParsedOperationParser().parse(input)
    }

    @Test
    fun parse_Transaction_SmartContract_Response() {
        val input =
            "[{\"branch\":\"BLjCqov6Sko6nMaKpbGn1ukub9JeKhyX837zZ3xPTihDeLFEsBC\",\"contents\":[{\"kind\":\"transaction\",\"source\":\"tz1RGPPkKmW1SsGVfKdEmTQU94xciCyQPqhB\",\"fee\":\"0\",\"counter\":\"832541\",\"gas_limit\":\"800000\",\"storage_limit\":\"60000\",\"amount\":\"0\",\"destination\":\"KT1NisAhyEtp2pKMh5ynVPhPBfwoGFHbMe6d\",\"parameters\":{\"entrypoint\":\"transfer\",\"value\":{\"prim\":\"Pair\",\"args\":[{\"prim\":\"Pair\",\"args\":[{\"string\":\"tz1RGPPkKmW1SsGVfKdEmTQU94xciCyQPqhB\"},{\"string\":\"tz1XSXBeWqdupm73qWAJkxJkxRzH16y77i1B\"}]},{\"int\":\"2\"}]}}}],\"signature\":\"edsigtXomBKi5CTRf5cjATJWSyaRvhfYNHqSUGrn4SdbYRcGwQrUGjzEfQDTuqHhuA8b2d8NarZjz8TRf65WkpQmo423BtomS8Q\"}]"
        val result = ParsedOperationParser().parse(input)
        println(input)

        Assert.assertNotNull(result)
        Assert.assertEquals(1, result!!.size)
        Assert.assertEquals(OperationType.TRANSACTION, result[0].type)
        Assert.assertTrue((result[0] as SmartContractCallOperation).parameter is PairMichelsonParameter)
        Assert.assertTrue((result[0] as SmartContractCallOperation).entrypoint == "transfer")
    }
}
