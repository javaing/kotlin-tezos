/**
 * # Released under MIT License
 *
 * Copyright (c) 2019 camlCase
 */
package io.camlcase.kotlintezos.test.data

import io.camlcase.kotlintezos.data.GetBigMapValueRPC
import io.camlcase.kotlintezos.data.RPC_PATH_BIG_MAP_SUFFIX
import io.camlcase.kotlintezos.data.blockchain.RPC_PATH_CONTRACTS
import io.camlcase.kotlintezos.smartcontract.michelson.MichelsonComparable
import io.camlcase.kotlintezos.smartcontract.michelson.StringMichelsonParameter
import io.camlcase.kotlintezos.test.util.TestnetAddress.Companion.TEST_ADDRESS_1
import io.camlcase.kotlintezos.test.util.TestnetAddress.Companion.TEST_ADDRESS_2
import org.junit.Assert
import org.junit.Test

class GetBigMapValueRPCTest {
    @Test
    fun testPayload() {
        val michelsonAddress = StringMichelsonParameter(TEST_ADDRESS_1)
        val expected =
            "{\"type\":{\"prim\":\"${MichelsonComparable.ADDRESS.name.toLowerCase()}\"},\"key\":{\"string\":\"$TEST_ADDRESS_1\"}}"

        val rpc = GetBigMapValueRPC(TEST_ADDRESS_2, michelsonAddress, MichelsonComparable.ADDRESS)
        Assert.assertEquals(RPC_PATH_CONTRACTS + TEST_ADDRESS_2 + RPC_PATH_BIG_MAP_SUFFIX, rpc.endpoint)
        Assert.assertEquals(expected, rpc.payload)
        Assert.assertTrue(rpc.header.count() > 0)
    }
}
