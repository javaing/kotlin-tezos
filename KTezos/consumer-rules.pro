# PROGUARD RULES for consumer

# OkHttp
# https://square.github.io/okhttp/r8_proguard/
# If you use OkHttp as a dependency in an Android project which uses R8 as a default compiler you
# don’t have to do anything. The specific rules are already bundled into the JAR which can be
# interpreted by R8 automatically.


# JNA
# https://github.com/java-native-access/jna/blob/master/www/FrequentlyAskedQuestions.md
-dontwarn java.awt.*
-keep class com.sun.jna.* { *; }
-keepclassmembers class * extends com.sun.jna.* { public *; }

# TrustWallet
# https://github.com/trustwallet/wallet-core/blob/05375deb1b4b45d491b59ca775aa43e9e9a39b11/samples/android/app/proguard-rules.pro
-keep class wallet.core.jni.** { *; }
-keep class wallet.core.jni.proto.** { *; }

# KotlinX Serialization
# https://github.com/Kotlin/kotlinx.serialization#android
-keepattributes *Annotation*, InnerClasses
-dontnote kotlinx.serialization.AnnotationsKt # core serialization annotations

# kotlinx-serialization-json specific. Add this if you have java.lang.NoClassDefFoundError kotlinx.serialization.json.JsonObjectSerializer
-keepclassmembers class kotlinx.serialization.json.** {
    *** Companion;
}
-keepclasseswithmembers class kotlinx.serialization.json.** {
    kotlinx.serialization.KSerializer serializer(...);
}

-keep,includedescriptorclasses class io.camlcase.kotlintezos.**$$serializer { *; }
-keepclassmembers class io.camlcase.kotlintezos.** {
    *** Companion;
}
-keepclasseswithmembers class io.camlcase.kotlintezos.** {
    kotlinx.serialization.KSerializer serializer(...);
}

 # Rhino
 -keep class org.mozilla.** { *; }
 -dontwarn org.mozilla.javascript.**
 -dontwarn org.mozilla.classfile.**
